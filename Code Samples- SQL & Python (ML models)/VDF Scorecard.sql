USE [DF_STAGE]
GO
/****** Object:  StoredProcedure [dbo].[SP_VDF_SCORECARD]    Script Date: 7/4/2019 3:44:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: Prashant Parikh
-- Description:	VDF SCORECARD
-- =============================================
ALTER PROCEDURE [dbo].[SP_VDF_SCORECARD] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

---BQ CODE
CREATE OR REPLACE TABLE `analytics-supplychain-thd.DF_IPR_BI.VDF_SCORECARD_SUMMARY`  AS (
SELECT  
      FW.FSCL_WK.FSCL_WK_END_DT
     ,FSCL_WK.FSCL_YR_WK_KEY_VAL
     , IDM_VNDR_ACCT_NM AS IDM_Supplier_Name    
     , PVEN.PVNDR_NBR AS PVNDR    
     , PVEN.PVNDR_NM AS PVNDR_Name   
   --, DIR_FULFL_ONLN_CLASS_VAL
   --, HDCOM_CHNL_STAT_DESC
     ,   CASE
              WHEN SUB_DEPT.SUB_DEPT_NBR IN ('025T', '025H',  '028I',  '028O')                THEN 'HARDLINES'
              WHEN SUB_DEPT.SUB_DEPT_NBR IN('023F',  '0024',  '059S', '029A', '029B')         THEN 'DECOR'
              WHEN SUB_DEPT.SUB_DEPT_NBR IN ('0027', '0027',  '0021', '0022', '0030', '026P') THEN 'BUILDING MATERIALS'
              ELSE 'UNKNOWN'
          END AS PROD_GRP_DESC
     , SUB_DEPT.SUB_DEPT_NBR AS  Sub_Dept_Number
     ,SUB_DEPT.SUB_DEPT_DESC AS Sub_Dept_Name
      
     , CLASS.CLASS_NBR AS Class
     , CLASS.CLASS_DESC AS Class_Name
     --,null  AS IPM
     ,DIR_FULFL_MGR_NM AS IPM
     ,CASE WHEN DFDRL_PRI_ITEM_DIM_ID = 1 THEN 'Y' ELSE 'N' END         AS Priority_SKU
     ,CASE WHEN DFDRL_R13W_PRITEM_WK_DIM_ID   = 13 THEN 'Y' ELSE 'N' END AS PRI_13
     , CASE WHEN FSCL_WK.FSCL_WK_END_DT >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY)
            THEN 'Y' ELSE 'N' END AS TW_IND
     , CASE WHEN FSCL_WK.FSCL_WK_END_DT >= DATE_SUB(CURRENT_DATE, INTERVAL 28 DAY) 
            THEN 'Y' 
            ELSE 'N' 
        END AS L4W_IND
    ,DIR_FULFL_ALST_NM AS IPR_Analyst
    ,CASE WHEN FOD.First_Online_Date IS NULL THEN 'NO' ELSE 'YES' END AS NEW_SKU
    , First_Online_Date
     , SUM(A.DSTNCT_PG_VIST_CNT) DSTNCT_PG_VIST_CNT
     , SUM(CUST_NOT_BUYBL_DAY_PG_VIST_CNT) CUST_NOT_BUYBL_DAY_PG_VIST_CNT
     , SUM(CUST_BUYBL_DAY_PG_VIST_CNT) CUST_BUYBL_DAY_PG_VIST_CNT
     , SUM(LOST_SLS_AMT) LOST_SLS_AMT
     , SUM(R3W_AVG_FCST_SLS_AMT) R3W_AVG_FCST_SLS_AMT
     , MAX(TV.DSTNCT_PG_VIST_CNT) COMP_TOTAL_VIEWS
     , SUM(CASE WHEN  COALESCE(CHUB_OH_QTY,0) > 0 AND FSCL_DAY.DAY_OF_WK_NBR = 7 
                THEN 1 
                ELSE 0 
            END) IS_CNT
     , SUM(CASE WHEN FSCL_DAY.DAY_OF_WK_NBR= 7 
                THEN 1 
                ELSE 0 
            END) TOTAL_CNT
     , SUM(CASE WHEN FSCL_DAY.DAY_OF_WK_NBR = 7 AND  COALESCE(CHUB_OH_QTY,0) > 0 AND  COALESCE(CHUB_OH_QTY,0) >= GREATEST(COALESCE(R4W_FCST_SLS_QTY,0),5)  
                THEN 1 
                ELSE 0 
            END) IS_4WKS 
     , SUM(CASE WHEN FSCL_DAY.DAY_OF_WK_NBR = 7 
                AND  COALESCE(CHUB_OH_QTY,0) > 0 
                AND  COALESCE(CHUB_OH_QTY,0) < GREATEST(COALESCE(R4W_FCST_SLS_QTY,0),5) 
                THEN 1 
                ELSE 0 
            END) IS_4WKS_BELOW 
     , SUM(CASE WHEN FSCL_DAY.DAY_OF_WK_NBR = 7  
                     AND COALESCE(CHUB_OH_QTY,0) = 0 
                     AND CHUB_NXT_EXPCTD_AVAIL_DT >= A.CAL_DT 
                     AND COALESCE(CHUB_NXT_EXPCTD_AVAIL_OH_QTY,0)>=GREATEST(COALESCE(R4W_FCST_SLS_QTY,0),5)  
                THEN 1 
                ELSE 0 
            END) OOS_NXT_AVAIL_DT_4WKS
     --, SUM(CASE WHEN DAY_OF_WK_NBR = 7  AND COALESCE(CHUB_OH_QTY,0) = 0 AND CHUB_NXT_EXPCTD_AVAIL_DT >= A.CAL_DT AND COALESCE(CHUB_NXT_EXPCTD_AVAIL_OH_QTY,0)=0  THEN 1 ELSE 0 END) OOS_NXT_AVAIL_DT_NO_FCST
     , SUM(CASE WHEN FSCL_DAY.DAY_OF_WK_NBR = 7  
                     AND COALESCE(CHUB_OH_QTY,0) = 0 
                     AND CHUB_NXT_EXPCTD_AVAIL_DT >= A.CAL_DT 
                     AND COALESCE(CHUB_NXT_EXPCTD_AVAIL_OH_QTY,0)<GREATEST(COALESCE(R4W_FCST_SLS_QTY,0),5)  
                THEN 1 
                ELSE 0
            END) OOS_NXT_AVAIL_DT_4WKS_LESS
     , SUM(CASE WHEN FSCL_DAY.DAY_OF_WK_NBR = 7  AND COALESCE(CHUB_OH_QTY,0) = 0 AND COALESCE(CHUB_NXT_EXPCTD_AVAIL_DT, DATE_SUB(CURRENT_DATE, INTERVAL 360 DAY) ) < A.CAL_DT 
                THEN 1 ELSE 0 END) OOS_NXT_AVAIL_DT_MISSING
  FROM `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL_DLY` A
       INNER JOIN `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL` B
                  ON A.DIR_FULFL_ITEM_DTL_ID = B.DIR_FULFL_ITEM_DTL_ID
        INNER JOIN `pr-edw-views-thd.SCHN_SHARED.DFDRL_ITEM_SHP_FR_DIM` SHP
                  ON CAST(A.DFDRL_ITEM_SHP_FR_DIM_ID AS STRING) = SHP.DFDRL_ITEM_SHP_FR_DIM_ID    
        INNER JOIN `pr-edw-views-thd.SHARED.CAL_PRD_HIER`  FW
                   ON A.CAL_DT = FW.CAL_DT
        LEFT OUTER JOIN `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_HIER` IH 
           ON IH.OMS_ITEM_ID = A.OMS_ITEM_ID   
      --  INNER JOIN `analytics-supplychain-thd.DF_IPR_BI.TBL_TOTAL_VIEWS` TV
      INNER JOIN (SELECT  FW.FSCL_WK_END_DT
       ,SUM(DSTNCT_PG_VIST_CNT) DSTNCT_PG_VIST_CNT
  FROM `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL_DLY` A
       INNER JOIN `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL` B
                   ON A.DIR_FULFL_ITEM_DTL_ID = B.DIR_FULFL_ITEM_DTL_ID
   
       INNER JOIN `pr-edw-views-thd.SCHN_SHARED.DFDRL_ITEM_SHP_FR_DIM` SHP
                 ON CAST(A.DFDRL_ITEM_SHP_FR_DIM_ID AS STRING) = SHP.DFDRL_ITEM_SHP_FR_DIM_ID    
       INNER JOIN `pr-edw-views-thd.SHARED.CAL_PRD_HIER_FD` FW
                 ON A.CAL_DT = FW.CAL_DT
WHERE 
    ITEM_SHP_FR_VAL = 'CHUB_OMS' 
     AND 
      A.DF_IPR_REPLE_DIM_ID = 1
       --AND B.ACTV_FLG = 'Y'
      AND 
       ( FW.FSCL_WK_END_DT BETWEEN  DATE_SUB(CURRENT_DATE, INTERVAL 91 DAY) AND CURRENT_DATE() 
                   OR FW.FSCL_WK_END_DT   BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 456 DAY)  AND DATE_SUB(CURRENT_DATE, INTERVAL 365 DAY) 
                   )
GROUP BY FW.FSCL_WK_END_DT) TV
                   ON FW.FSCL_WK.FSCL_WK_END_DT	 = TV.FSCL_WK_END_DT
        LEFT OUTER JOIN (SELECT * 
                           FROM `pr-edw-views-thd.SHARED_BASE_VNDR.PVNDR` 
                          WHERE ACTV_FLG = true) PVEN    
                         ON B.ONLN_PVNDR_NBR = PVEN.PVNDR_NBR
LEFT OUTER JOIN(
SELECT * FROM (SELECT
X4.*
,RANK()OVER (PARTITION BY SKU_NBR ORDER BY SKU.SKU_STAT_CD DESC, SKU.SKU_STAT_DESC ASC, LAST_UPD_TS DESC) AS RNK
FROM   `pr-edw-views-thd.SHARED.SKU_HIER`X4
) A
WHERE RNK = 1
) X4
on  B.SO_SKU_NBR = X4.SKU_NBR

LEFT JOIN  ( select OMS_ID, MIN(CAST(First_Online_Date AS DATETIME)) AS First_Online_Date
from `pr-edw-views-thd.ONLN_SPOT.PRODUCT`
WHERE First_Online_Date <> "UNKNOWN"
AND DATE_DIFF(CURRENT_DATE, CAST(CAST(First_Online_Date AS DATETIME) AS DATE) , DAY) <= 547
and  CHANNEL_STATUS LIKE '%DOTCOM_CERTIFIED%' GROUP BY 1) FOD
                 ON A.OMS_ITEM_ID = FOD.OMS_ID
            


WHERE ITEM_SHP_FR_VAL = 'CHUB_OMS' 
      AND A.DF_IPR_REPLE_DIM_ID = 1
      AND (FW.FSCL_WK.FSCL_WK_END_DT BETWEEN  DATE_SUB(CURRENT_DATE, INTERVAL 91 DAY) AND CURRENT_DATE
                       OR FW.FSCL_WK.FSCL_WK_END_DT  BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 456 DAY)  AND DATE_SUB(CURRENT_DATE, INTERVAL 365 DAY)
           )
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12, 13,14,15,16,17,18

);

-----------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE TABLE `analytics-supplychain-thd.DF_IPR_BI.VDF_SCORECARD_DETAILS` AS (
SELECT FW.FSCL_WK.FSCL_WK_END_DT
      , IDM_VNDR_ACCT_NM AS IDM_Supplier_NAME --"IDM Supplier NAME"    
      , PVEN.PVNDR_NBR AS PVNDR --"PVNDR"    
      , PVEN.PVNDR_NM AS PVNDR_NAME --"PVNDR NAME"    
      , MFR_MDL_NBR AS SUPPLIER_SKU --"SUPPLIER SKU"
      , CONCAT(SUB_DEPT.SUB_DEPT_NBR  , ' - ' ,SUB_DEPT.SUB_DEPT_DESC) AS DEPT
      --CONCAT(first_name, " ", last_name)
      , CONCAT(CASE WHEN LENGTH(TRIM(CAST(CLASS.CLASS_NBR AS STRING))) = 1 THEN '0' ELSE '' END , TRIM(CAST(CLASS.CLASS_NBR AS STRING)) , ' - ' , TRIM(CAST(CLASS.CLASS_DESC AS STRING))) CLASS --"CLASS"
      ,  CASE
              WHEN SUB_DEPT.SUB_DEPT_NBR IN ('025T', '025H',  '028I',  '028O')                THEN 'HARDLINES'
              WHEN SUB_DEPT.SUB_DEPT_NBR IN('023F',  '0024',  '059S', '029A', '029B')         THEN 'DECOR'
              WHEN SUB_DEPT.SUB_DEPT_NBR IN ('0027', '0027',  '0021', '0022', '0030', '026P') THEN 'BUILDING MATERIALS'
              ELSE 'UNKNOWN'
          END AS GRP_DESC
      , SUB_DEPT.SUB_DEPT_NBR AS Sub_Dept_Number --"Sub Dept Number"
      , SUB_DEPT.SUB_DEPT_DESC as Sub_Dept_Name --AS "Sub Dept Name"
      , CLASS.CLASS_NBR  Class2 --AS "Class2"
      , CLASS.CLASS_DESC Class_Name --AS "Class Name"
      ,DIR_FULFL_MGR_NM AS IPM
         ,CASE WHEN DFDRL_PRI_ITEM_DIM_ID = 1 THEN 'Y' ELSE 'N' END   Priority_SKU   --   AS "Priority SKU"
         ,CASE WHEN DFDRL_R13W_PRITEM_WK_DIM_ID   = 13 THEN 'Y' ELSE 'N' END  PRI_13  -- AS "PRI_13"
      --,can you find the equivalent of this in the DIR_FULFL_ALST_NM AS IPR_Analyst --"IPR Analyst"
      ,DIR_FULFL_ALST_NM AS IPR_Analyst
      , A.OMS_ITEM_ID
      , B.OMS_DESC     
      --, DIR_FULFL_ONLN_CLASS_VAL
      --, HDCOM_CHNL_STAT_DESC AS "CHUB AVAILABILITY STATUS"
      , CHUB_AVAIL_IND_VAL CHUB_AVAILABILITY_STATUS--AS "CHUB AVAILABILITY STATUS"
      , CHUB_OH_QTY CHUB_AVAIL_OH --AS "CHUB AVAIL OH"
      , CHUB_NXT_EXPCTD_AVAIL_DT CHUB_NEXT_AVAIL_DATE--AS "CHUB NEXT AVAIL DATE"
      , CHUB_NXT_EXPCTD_AVAIL_OH_QTY CHUB_NEXT_AVAIL_UNITS--AS "CHUB NEXT AVAIL UNITS"
      , GREATEST(COALESCE(R4W_FCST_SLS_QTY,0),5)TARGET_OH --AS "TARGET OH"
      , R4W_FCST_SLS_QTY WEBSITE_4W_FORECAST_UNITS --AS "WEBSITE - 4W FORECAST UNITS"
      , CASE WHEN (GREATEST(COALESCE(R4W_FCST_SLS_QTY,0),5) - COALESCE(CHUB_OH_QTY,0)) > 0 
             THEN (GREATEST(COALESCE(R4W_FCST_SLS_QTY,0),5) - COALESCE(CHUB_OH_QTY,0)) 
             ELSE 0 
         END INVENTORY_SHORTAGE_UNITS -- AS "INVENTORY SHORTAGE UNITS"
      , A2.DSTNCT_PG_VIST_CNT
      , A2.CUST_NOT_BUYBL_DAY_PG_VIST_CNT
      , A2.CUST_BUYBL_DAY_PG_VIST_CNT
      , A2.LOST_SLS_AMT
      --, CASE WHEN (A.CAL_DT - A3.LAST_OH_DT) = 1 THEN '-'
      --                WHEN (A.CAL_DT - A3.LAST_OH_DT) IS NULL THEN 'MORE THAN 1 YEAR'
      --                ELSE (A.CAL_DT - A3.LAST_OH_DT)   END AS "CONSECUTIVE OOS DAYS" 
      
      ,DATE_DIFF(A.CAL_DT, A.OOS_DT, DAY) CONSECUTIVE_OOS_DAYS --AS "CONSECUTIVE OOS DAYS" 
      --SELECT DATE_DIFF(DATE '2010-07-07', DATE '2008-12-25', DAY) as days_diff;    DATA SUB USED HERE 
      
      --, A4.LOST_SLS_AMT "LOST SALES SICNE OOS"
      --, A4.CUST_NOT_BUYBL_DAY_PG_VIST_CNT CUST_NOT_BUYBL_DAY_PG_VIST_CNT_SNC_OOS
      --, A4.CUST_BUYBL_DAY_PG_VIST_CNT CUST_BUYBL_DAY_PG_VIST_CNT_SNC_OOS
      --, A4.DSTNCT_PG_VIST_CNT DSTNCT_PG_VIST_CNT_SNC_OOS
      , CASE WHEN FOD.First_Online_Date IS NULL THEN 'NO' ELSE 'YES' END AS NEW_SKU
      , First_Online_Date
      , CASE WHEN A2.DSTNCT_PG_VIST_CNT > 0 THEN A2.CUST_BUYBL_DAY_PG_VIST_CNT*1.00/A2.DSTNCT_PG_VIST_CNT  ELSE 0.00 END LW_SERVICE_LEVEL_Percentage --AS "LW SERVICE LEVEL %"
      , CASE WHEN COALESCE(CHUB_OH_QTY,0) > 0 AND COALESCE(CHUB_OH_QTY,0) >= GREATEST(COALESCE(R4W_FCST_SLS_QTY,0),5)  THEN 'IS_4WKS'
             WHEN COALESCE(CHUB_OH_QTY,0) > 0 AND COALESCE(CHUB_OH_QTY,0) < GREATEST(COALESCE(R4W_FCST_SLS_QTY,0),5) THEN 'IS_4WKS_BELOW' 
             WHEN COALESCE(CHUB_OH_QTY,0) = 0 AND CHUB_NXT_EXPCTD_AVAIL_DT >= A.CAL_DT AND COALESCE(CHUB_NXT_EXPCTD_AVAIL_OH_QTY,0)>=GREATEST(COALESCE(R4W_FCST_SLS_QTY,0),5) 
             THEN 'OOS_NXT_AVAIL_DT_4WKS'
           --WHEN COALESCE(CHUB_OH_QTY,0) = 0 AND CHUB_NXT_EXPCTD_AVAIL_DT >= A.CAL_DT AND COALESCE(CHUB_NXT_EXPCTD_AVAIL_OH_QTY,0)=0  THEN 'OOS_NXT_AVAIL_DT_NO_FCST'
             WHEN COALESCE(CHUB_OH_QTY,0) = 0 AND CHUB_NXT_EXPCTD_AVAIL_DT >= A.CAL_DT AND COALESCE(CHUB_NXT_EXPCTD_AVAIL_OH_QTY,0)<GREATEST(COALESCE(R4W_FCST_SLS_QTY,0),5)  
             THEN 'OOS_NXT_AVAIL_DT_4WKS_LESS'
             WHEN COALESCE(CHUB_OH_QTY,0) = 0 AND COALESCE(CHUB_NXT_EXPCTD_AVAIL_DT, DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY) ) < A.CAL_DT  THEN 'OOS_NXT_AVAIL_DT_MISSING' 
         END INVENTORY_STATUS  --AS "INVENTORY STATUS"
         
FROM `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL_DLY` A
    LEFT OUTER JOIN (SELECT OMS_ITEM_ID, 
                            COUNT(*) DAY_CNT,
                            SUM(LOST_SLS_AMT) LOST_SLS_AMT, 
                            SUM(DSTNCT_PG_VIST_CNT) DSTNCT_PG_VIST_CNT, 
                            SUM(CUST_NOT_BUYBL_DAY_PG_VIST_CNT) CUST_NOT_BUYBL_DAY_PG_VIST_CNT, 
                            SUM(CUST_BUYBL_DAY_PG_VIST_CNT) CUST_BUYBL_DAY_PG_VIST_CNT  
                       FROM `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL_DLY` A
                            INNER JOIN `pr-edw-views-thd.SHARED.CAL_PRD_HIER` B
                                       ON A.CAL_DT = B.CAL_DT
                      WHERE B.FSCL_WK.FSCL_WK_END_DT  BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY) AND CURRENT_DATE  
                            AND DF_IPR_REPLE_DIM_ID = 1 GROUP BY 1) A2    
                    ON A.OMS_ITEM_ID = A2.OMS_ITEM_ID
    INNER JOIN `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL` B
               ON A.DIR_FULFL_ITEM_DTL_ID = B.DIR_FULFL_ITEM_DTL_ID  
               --DIR FULFIL ITEM DTL HIER on A.DIR_FULFL_ITEM_DTL_ID  
    INNER JOIN `pr-edw-views-thd.SCHN_SHARED.DFDRL_ITEM_SHP_FR_DIM` SHP
               ON CAST(A.DFDRL_ITEM_SHP_FR_DIM_ID AS STRING) = SHP.DFDRL_ITEM_SHP_FR_DIM_ID    
    INNER JOIN `pr-edw-views-thd.SHARED.CAL_PRD_HIER` FW
               ON A.CAL_DT = FW.CAL_DT
              
    LEFT OUTER JOIN (SELECT * 
                       FROM `pr-edw-views-thd.SHARED_BASE_VNDR.PVNDR` 
                      WHERE ACTV_FLG = true) PVEN    
                            ON B.ONLN_PVNDR_NBR = PVEN.PVNDR_NBR
LEFT OUTER JOIN(
SELECT * FROM (SELECT
X4.*
,RANK()OVER (PARTITION BY SKU_NBR ORDER BY SKU.SKU_STAT_CD DESC, SKU.SKU_STAT_DESC ASC, LAST_UPD_TS DESC) AS RNK
FROM   `pr-edw-views-thd.SHARED.SKU_HIER`X4
) A
WHERE RNK = 1
) X4
on  B.SO_SKU_NBR = X4.SKU_NBR
    LEFT OUTER JOIN `pr-edw-views-thd.SCHN_SHARED.DFDRL_CHUB_AVAIL_DIM` I
                    ON CAST(I.DFDRL_CHUB_AVAIL_DIM_ID AS INT64) = A.DFDRL_CHUB_AVAIL_DIM_ID
    LEFT OUTER JOIN `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_HIER` IH 
                   ON IH.OMS_ITEM_ID = A.OMS_ITEM_ID  
                   
    LEFT JOIN  ( select OMS_ID, MIN (CAST(First_Online_Date AS DATETIME)) AS First_Online_Date
from `pr-edw-views-thd.ONLN_SPOT.PRODUCT`
WHERE First_Online_Date <> "UNKNOWN"
AND DATE_DIFF(CURRENT_DATE, CAST(CAST(First_Online_Date AS DATETIME) AS DATE) , DAY) <= 547
and  CHANNEL_STATUS LIKE '%DOTCOM_CERTIFIED%' GROUP BY 1) FOD
                 ON A.OMS_ITEM_ID = FOD.OMS_ID
                   
WHERE  ITEM_SHP_FR_VAL  = 'CHUB_OMS' 
       AND A.DF_IPR_REPLE_DIM_ID = 1
       AND A.CAL_DT  IN (SELECT DISTINCT FSCL_WK.FSCL_WK_END_DT 
                           FROM  `pr-edw-views-thd.SHARED.CAL_PRD_HIER` B
                          WHERE FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL "7" DAY) 
                                AND CURRENT_DATE)


);

------------------ SQL SERVER 
-----------
---------------
-----------------



	PRINT 'Starting - VDF Details'

IF OBJECT_ID('DF_STAGE.DBO.VDF_SCORECARD_DETAILS_2', 'U') IS NOT NULL
	drop table DF_STAGE.DBO.VDF_SCORECARD_DETAILS_2;
declare @src_qry as varchar(max)
set @src_qry = 

'SELECT * FROM `analytics-supplychain-thd.DF_IPR_BI.VDF_SCORECARD_DETAILS`
' 
exec DF_STAGE.dbo.usp_import_data_from_BigQuery
       @src_qry = @src_qry
,@dest_tbl = 'DF_STAGE.DBO.VDF_SCORECARD_DETAILS_2'
,@crt_dest_tbl = 1;

PRINT 'Ending - VDF Details'




PRINT 'Starting - VDF Summary'

IF OBJECT_ID('DF_STAGE.DBO.VDF_SCORECARD_SUMMARY', 'U') IS NOT NULL
	drop table DF_STAGE.DBO.VDF_SCORECARD_SUMMARY;

set @src_qry = 

'SELECT * FROM `analytics-supplychain-thd.DF_IPR_BI.VDF_SCORECARD_SUMMARY`
' 
exec DF_STAGE.dbo.usp_import_data_from_BigQuery
       @src_qry = @src_qry
,@dest_tbl = 'DF_STAGE.DBO.VDF_SCORECARD_SUMMARY'
,@crt_dest_tbl = 1;

PRINT 'Ending - VDF summary'


--IF OBJECT_ID('DF_STAGE.DBO.REV_OPS', 'U') IS NOT NULL
--	drop table DF_STAGE.DBO.REV_OPS;

--EXEC SSISDB.EDIS.usp_excel_import_data
-- @file_path = 'F:\IPR-SSIS\REV_OPS.xlsx'
-- ,@sheet_nm = 'Sheet1'
-- ,@include_headers = 1
-- ,@dest_tbl_nm = 'DF_STAGE.DBO.REV_OPS'
-- ,@crt_dest_tbl = 1

DROP TABLE DF_STAGE.DBO.VDF_SCORECARD_DETAILS_NEW

SELECT A.*, B.[Users Tracking] 
INTO DF_STAGE.DBO.VDF_SCORECARD_DETAILS_NEW
FROM DF_STAGE.DBO.VDF_SCORECARD_DETAILS_2 A
LEFT JOIN
df_stage.dbo.REV_OPS B --receive mondays... take file from kirti...remove description column
ON A.OMS_ITEM_ID = B.[OMS ID]

DROP TABLE DF_STAGE.DBO.VDF_SCORECARD_DETAILS

SELECT * INTO DF_STAGE.DBO.VDF_SCORECARD_DETAILS
FROM DF_STAGE.DBO.VDF_SCORECARD_DETAILS_NEW 


IF OBJECT_ID('tempdb..##IPR_CONTACT') IS NOT NULL
DROP TABLE ##IPR_CONTACT


SELECT DISTINCT [PVNDR], AA.[IPR_ANALYST], LTRIM(RTRIM(B.EMAIL))AS EMAIL, LTRIM(RTRIM(coalesce(C.EMAIL,''))) AS IPM_EMAIL, RW
INTO ##IPR_CONTACT 
FROM (
	SELECT [IPR_ANALYST], IPM, [PVNDR], OOS
			,ROW_NUMBER() OVER(PARTITION BY [IPR_ANALYST] ORDER BY OOS DESC) AS RW
			FROM 
			(SELECT [IPR_ANALYST], IPM,  [PVNDR]
				, SUM(A.CUST_NOT_BUYBL_DAY_PG_VIST_CNT)*10000.0000/MAX(COMP_TOTAL_VIEWS) AS OOS
			 FROM DF_STAGE.DBO.VDF_SCORECARD_SUMMARY A
			WHERE FSCL_WK_END_DT >= GETDATE() - 8
			GROUP BY [IPR_ANALYST], IPM ,[PVNDR] ) AS A
	 ) AS AA
LEFT JOIN DF_STAGE.DBO.[VDF_IPR_CONTACTS] B
ON AA.[IPR_ANALYST] = B.IPR_ANALYST
LEFT JOIN DF_STAGE.DBO.[VDF_IPR_CONTACTS] C
ON AA.[IPM] = C.IPR_ANALYST
WHERE RW <= 5000 AND AA.[IPR_ANALYST] IS NOT NULL  AND PVNDR IS NOT NULL
ORDER BY [PVNDR], RW ;


IF OBJECT_ID('tempdb..##IPR_CONTACT_w_EMAIL') IS NOT NULL
DROP TABLE ##IPR_CONTACT_w_EMAIL

Select
 distinct ST2.[PVNDR], 
    substring(
        (
            Select ';'+LTRIM(RTRIM(ST1.EMAIL))  AS [text()]
			From (SELECT DISTINCT PVNDR,  EMAIL FROM ##IPR_CONTACT) ST1
            Where ST1.[PVNDR] = ST2.[PVNDR]
            ORDER BY ST1.[PVNDR]
            For XML PATH ('')
        ), 2, 10000) [EMAILS]
	,    substring(
        (
            Select ';'+LTRIM(RTRIM(ST1.IPM_EMAIL))  AS [text()]
			From (SELECT DISTINCT PVNDR,  IPM_EMAIL FROM ##IPR_CONTACT) ST1
            Where ST1.[PVNDR] = ST2.[PVNDR]
            ORDER BY ST1.[PVNDR]
            For XML PATH ('')
        ), 2, 10000) [IPM_EMAILS]
	,    substring(
        (
           Select ';'+ LTRIM(RTRIM(ST1.EMAIL)) AS [text()]
            From ##IPR_CONTACT ST1
            Where ST1.[PVNDR] = ST2.[PVNDR]
            ORDER BY ST1.[PVNDR]
            For XML PATH ('')
        ), 2, 10000) [PRI_EMAILS]
into ##IPR_CONTACT_w_EMAIL
From ##IPR_CONTACT ST2;



----Get IPR Analysts to be in the body of the email-----


IF OBJECT_ID('tempdb..##IPR_PRIM_CONTACT') IS NOT NULL
DROP TABLE ##IPR_PRIM_CONTACT

SELECT DISTINCT [PVNDR],AA.[IPR_ANALYST], B.EMAIL
INTO ##IPR_PRIM_CONTACT 
FROM (
	SELECT [IPR_ANALYST], [PVNDR], SKU_CNT
			,ROW_NUMBER() OVER(PARTITION BY [PVNDR] ORDER BY SKU_CNT DESC) AS RW
			FROM 
			(SELECT [IPR_ANALYST], [PVNDR]
				, SUM(TOTAL_CNT) AS SKU_CNT
			 FROM DF_STAGE.DBO.VDF_SCORECARD_SUMMARY A
			WHERE FSCL_WK_END_DT >= GETDATE() - 8
			GROUP BY [IPR_ANALYST], [PVNDR]) AS A
	 ) AS AA
LEFT JOIN DF_STAGE.[dbo].[VDF_IPR_CONTACTS] B
ON AA.[IPR_ANALYST] = B.IPR_ANALYST
WHERE RW = 1 AND AA.[IPR_ANALYST] IS NOT NULL
ORDER BY [PVNDR] ;


IF OBJECT_ID('tempdb..##CONTACTS') IS NOT NULL
DROP TABLE ##CONTACTS



declare @SQL as VARCHAR(MAX) = 'SELECT 
PRTHD.MVNDR.PVNDR_NBR
, PRTHD.PVNDR.PVNDR_NM
--, PRTHD.MVNDR.MVNDR_NBR
--, PRTHD.MVNDR.MVNDR_NM
--, PRTHD.N_SUPLR_CNTCT_TYP.S_TYP_DESC AS Key_Contact
--, PRTHD.SYSUSR.ACTV_FLG
--, PRTHD.MVNDR.MVNDR_STAT_CD
--, PRTHD.PRTL_USER.GDN_FLG
--, PRTHD.SUPLR_CNTCT.CNTCT_FRST_NM
--, PRTHD.SUPLR_CNTCT.CNTCT_LAST_NM
, PRTHD.SUPLR_CNTCT.EMAIL_ADDR_TXT
, PRTHD.N_EBPRTNR_TYP.S_TYP_DESC
, PRTHD.N_SUPLR_CNTCT_TYP.SUPLR_CNTCT_TYP_CD
FROM (((((((PRTHD.PVNDR 
			INNER JOIN PRTHD.MVNDR 
			ON PRTHD.PVNDR.PVNDR_NBR = PRTHD.MVNDR.PVNDR_NBR) 
			INNER JOIN ((PRTHD.SUPLR_CNTCT INNER JOIN PRTHD.SUPLR_CNTCT_PHN 
							ON PRTHD.SUPLR_CNTCT.SUPLR_CNTCT_ID = PRTHD.SUPLR_CNTCT_PHN.SUPLR_CNTCT_ID) 
						INNER JOIN PRTHD.PRTL_USER 
							ON PRTHD.SUPLR_CNTCT.SUPLR_CNTCT_ID = PRTHD.PRTL_USER.SUPLR_CNTCT_ID) 
				ON PRTHD.MVNDR.PVNDR_NBR = PRTHD.PRTL_USER.PVNDR_NBR) 
			INNER JOIN PRTHD.SYSUSR 
				ON PRTHD.SUPLR_CNTCT.SUPLR_CNTCT_ID = PRTHD.SYSUSR.SUPLR_CNTCT_ID) 
			INNER JOIN PRTHD.MVNDR_SUPLR_CNTCT 
				ON (PRTHD.MVNDR.MER_DEPT_NBR = PRTHD.MVNDR_SUPLR_CNTCT.MER_DEPT_NBR) 
				AND (PRTHD.MVNDR.MVNDR_NBR = PRTHD.MVNDR_SUPLR_CNTCT.MVNDR_NBR)) 
			INNER JOIN PRTHD.N_SUPLR_CNTCT_TYP 
				ON PRTHD.MVNDR_SUPLR_CNTCT.SUPLR_CNTCT_TYP_CD = PRTHD.N_SUPLR_CNTCT_TYP.SUPLR_CNTCT_TYP_CD) 
			INNER JOIN PRTHD.EBPRTNR_TYP_ASSG 
				ON PRTHD.PVNDR.PVNDR_NBR = PRTHD.EBPRTNR_TYP_ASSG.PVNDR_NBR) 
			INNER JOIN PRTHD.N_EBPRTNR_TYP 
				ON PRTHD.EBPRTNR_TYP_ASSG.EBPRTNR_TYP_ID = PRTHD.N_EBPRTNR_TYP.EBPRTNR_TYP_ID) 
			INNER JOIN PRTHD.MVNDR_TYP ON PRTHD.MVNDR.MVNDR_TYP_CD = PRTHD.MVNDR_TYP.MVNDR_TYP_CD
		WHERE (((PRTHD.SYSUSR.ACTV_FLG)=''Y'') AND ((PRTHD.MVNDR.MVNDR_STAT_CD)=100) AND ((PRTHD.N_EBPRTNR_TYP.S_TYP_DESC)=''US MERCHANDISING'') AND ((PRTHD.N_SUPLR_CNTCT_TYP.SUPLR_CNTCT_TYP_CD) In (1,6,9,12)))
		GROUP BY PRTHD.MVNDR.PVNDR_NBR, PRTHD.PVNDR.PVNDR_NM
			--, PRTHD.MVNDR.MVNDR_NBR, PRTHD.MVNDR.MVNDR_NM, PRTHD.N_SUPLR_CNTCT_TYP.S_TYP_DESC, PRTHD.SYSUSR.ACTV_FLG, PRTHD.MVNDR.MVNDR_STAT_CD, PRTHD.PRTL_USER.GDN_FLG, PRTHD.SUPLR_CNTCT.CNTCT_FRST_NM, PRTHD.SUPLR_CNTCT.CNTCT_LAST_NM
			, PRTHD.SUPLR_CNTCT.EMAIL_ADDR_TXT, PRTHD.N_EBPRTNR_TYP.S_TYP_DESC, PRTHD.N_SUPLR_CNTCT_TYP.SUPLR_CNTCT_TYP_CD
		
';

			EXEC SSISDB.EDIS.usp_run_data_transfer @src_sys =  'PR1' 
								, @dest_sys = @@SERVERNAME
								, @src_qry = @SQL
								, @dest_tbl = '##CONTACTS_STG'
								, @crt_dest_tbl = 1;



IF OBJECT_ID('tempdb..##CONTACTS') IS NOT NULL
DROP TABLE ##CONTACTS	

SELECT DISTINCT PVNDR_NBR,PVNDR_NM,EMAIL_ADDR_TXT
INTO ##CONTACTS
FROM ##CONTACTS_STG




IF OBJECT_ID('tempdb..##VENDOR_Email') IS NOT NULL
DROP TABLE ##VENDOR_Email


Select
 distinct ST2.PVNDR_NBR, ST2.PVNDR_NM,
    substring(
        (
            Select ';'+ST1.EMAIL_ADDR_TXT  AS [text()]
            From ##Contacts ST1
            Where ST1.PVNDR_NBR = ST2.PVNDR_NBR
            ORDER BY ST1.PVNDR_NM
            For XML PATH ('')
        ), 2, 10000) [EMAILS]
into ##VENDOR_Email
From ##Contacts ST2



IF OBJECT_ID('tempdb..##VENDOR_Email_Final') IS NOT NULL
DROP TABLE ##VENDOR_Email_Final


SELECT * 
INTO ##VENDOR_Email_FINAL
FROM (
SELECT * FROM ##VENDOR_Email WHERE PVNDR_NBR not in (select distinct PVENDOR_NBR FROM VDF_VENDOR_CONTACTS)
UNION ALL
SELECT PVENDOR_NBR AS PVNDR_NBR, '' AS PVNDR_NM, EMAIL AS EMAILS FROM VDF_VENDOR_CONTACTS WHERE EMAIL <> 'DNS'
) AS S



DROP TABLE DF_STAGE.dbo.VDF_CONTACTS;
SELECT a.PVendor
	,'' IPR_PRIM_FULL_NAME
	,COALESCE(C.PRI_EMAILS, '') IPR_PRIM_EMAIL
	--,C.IPR_ANALYST IPR_TO_CC_FULL_NAME
	,COALESCE(C.EMAILS, '') + ';' +  COALESCE(C.IPM_EMAILS, '')  IPR_TO_CC_EMAIL
	,D.EMAILS VENDOR_TO_EMAILS
INTO DF_STAGE.dbo.VDF_CONTACTS
FROM
	(SELECT DISTINCT [PVNDR], [PVNDR_NAME], (convert(nvarchar(max), [PVNDR_NAME]) + ' - ' + convert(nvarchar(max), [PVNDR])) PVendor   FROM DF_STAGE.DBO.VDF_SCORECARD_SUMMARY  WHERE FSCL_WK_END_DT >= GETDATE() - 7) A
	--LEFT JOIN ##IPR_PRIM_CONTACT B
	--ON A.[PVNDR] = B.[PVNDR]
	LEFT JOIN ##IPR_CONTACT_w_EMAIL C
	ON A.[PVNDR] = C.[PVNDR]
	LEFT JOIN ##VENDOR_Email_FINAL D
	ON A.[PVNDR] = D.PVNDR_NBR

/*
--For Emails 
SELECT [PVendor], VENDOR_TO_EMAILS, IPR_TO_CC_EMAIL, 'karthikeyan_duraisamy@homedepot.com' as BCC, IPR_PRIM_FULL_NAME,IPR_PRIM_EMAIL  
FROM df_stage.dbo.VDF_CONTACTS 
WHERE 1=1
AND (VENDOR_TO_EMAILS IS NOT NULL OR VENDOR_TO_EMAILS <> '')
-- AND VENDOR_TO_EMAILS LIKE 'DNS'
--AND PvENDOR LIKE '%85903%'                                                                                             
ORDER BY [PVendor]
*/


DROP TABLE DF_STAGE.DBO.VDF_TOP_5_Vendors;
 --Vendor List
SELECT distinct  -1 AS [SELECTED],
PVendor as MVNDR_NM
INTO DF_STAGE.DBo.VDF_TOP_5_Vendors
FROM (
	SELECT IPR_ANALYST, PVendor, OOS
			,ROW_NUMBER() OVER(PARTITION BY IPR_ANALYST ORDER BY OOS DESC) AS RW
			FROM 
			(SELECT IPR_ANALYST, (convert(nvarchar(100), [PVNDR_NAME]) + ' - ' + convert(nvarchar(100), [PVNDR])) PVendor
				, SUM(A.CUST_NOT_BUYBL_DAY_PG_VIST_CNT)*10000.0000/MAX(COMP_TOTAL_VIEWS) AS OOS
			 FROM DF_STAGE.DBO.VDF_SCORECARD_SUMMARY  A
			WHERE FSCL_WK_END_DT >= GETDATE() - 7 --and [PVNDR_NAME] Like '%Quinco%'
			GROUP BY IPR_ANALYST, (convert(nvarchar(100), [PVNDR_NAME]) + ' - ' + convert(nvarchar(100), [PVNDR]))) AS A
	 ) AS AA
WHERE 1=1 
AND RW <=5
AND PVendor IS NOT NULL
AND PVENDOR IN (SELECT DISTINCT PVENDOR FROM DF_STAGE.dbo.VDF_CONTACTS); --WHERE IPR_TO_CC_EMAIL not LIKE '%@%' AND PVENDOR IS NOT NULL)
--AND PVENDOR NOT IN ()
--SELECT * FROM Ad_hoc.DBo.VDF_TOP_5_Vendors where MVNDR_NM not like '%DNU%'

DROP TABLE DF_STAGE.DBO.VDF_ALL_Vendors ;

SELECT distinct -1 AS [SELECTED],
PVendor as  [MVNDR_NM] 
INTO DF_STAGE.DBo.VDF_ALL_Vendors 
FROM (
	SELECT IPR_ANALYST, PVendor, OOS
			,ROW_NUMBER() OVER(PARTITION BY IPR_ANALYST ORDER BY OOS DESC) AS RW
			FROM 
			(SELECT IPR_ANALYST, (convert(nvarchar(100), [PVNDR_NAME]) + ' - ' + convert(nvarchar(100), [PVNDR])) PVendor
				, SUM(A.CUST_NOT_BUYBL_DAY_PG_VIST_CNT)*10000.0000/MAX(COMP_TOTAL_VIEWS) AS OOS
			 FROM DF_STAGE.DBO.VDF_SCORECARD_SUMMARY  A
			WHERE FSCL_WK_END_DT >= GETDATE() - 8
			AND PVNDR IN (SELECT DISTINCT PVNDR_NBR FROM ##VENDOR_Email_FINAL)
			GROUP BY IPR_ANALYST, (convert(nvarchar(100), [PVNDR_NAME]) + ' - ' + convert(nvarchar(100), [PVNDR]))) AS A
	 ) AS AA
WHERE pvendor is not null AND OOS > 0
and PVendor not in (SELECT DISTINCT MVNDR_NM FROM DF_STAGE.DBo.VDF_TOP_5_Vendors);

--SELECT * FROM Ad_hoc.DBo.VDF_ALL_Vendors where MVNDR_NM not like '%DNU%' and MVNDR_NM <= 'Z' order by MVNDR_NM



END
