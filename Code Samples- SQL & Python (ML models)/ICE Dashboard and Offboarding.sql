	
CREATE OR REPLACE TABLE  `analytics-supplychain-thd.AAA_TEMP.ICE_DASHBOARD_ATTR_21`
AS (SELECT B.CAL_DT 
           , ITM.SKU_NBR AS YOW_ITEM_SKU_NBR
           , ITM.SKU_DESC AS YOW_ITEM_DESC
           , G.MVNDR_NBR
           , G.MVNDR_NM
           , F.FIRST_RECPT_DT
           , F.LAST_RECPT_DT 
           , A.ITEM_ID AS REL_THD_ITEM_ID
           , E.SUB_DEPT.SUB_DEPT_NBR
           , E.SUB_DEPT.SUB_DEPT_DESC
           , E.CLASS.CLASS_NBR
           , E.CLASS.CLASS_DESC
           , E.SUB_CLASS.SUB_CLASS_NBR
           , E.SUB_CLASS.SUB_CLASS_DESC
           , N.SKU_STAT_CD
           , ITEM_VLCTY_DESC 
           , CASE 
                 WHEN N.SKU_STAT_CD IN (400,600) THEN 'I'
                 WHEN N.SKU_STAT_CD IN (500) THEN 'C'
                 WHEN N.SKU_STAT_CD IN (100,200) AND A.ITEM_VLCTY_CD = 5 THEN 'E'
                 WHEN N.SKU_STAT_CD IN (100,200) AND A.ITEM_VLCTY_CD <> 5 THEN 'Active Overstock'
                 ELSE 'NOT ICE' 
             END AS ICE_CD

       ,count (distinct concat(cast(A.ITEM_ID as string) , cast(B.DC_NBR  as string))) AS SKU_DC_CNT
          , SUM(B.OH_QTY) OH_QTY
          , SUM(B.OH_QTY * N.CURR_COST_AMT) AS  OH_COST_AMT
          , SUM(B.OH_QTY * B.CURR_RETL_AMT) AS  OH_RETL_AMT
          , 0 AS PPS_FLG
     FROM `pr-edw-views-thd.SCHN_INV.DC_INV_DLY`  B
           LEFT JOIN `pr-edw-views-thd.TD_COPIES.ITEM_HIER_MAPPING` ITEM
                      ON B.SKU_NBR = ITEM.ITEM_SKU_NBR
           LEFT JOIN `pr-edw-views-thd.SCHN_INV.YOW_ITEM_VLCTY`  AS A
                      ON A.ITEM_ID = ITEM.ITEM_ID
                         AND A.FSCL_WK_END_DT = B.CAL_DT    
          INNER JOIN `pr-edw-views-thd.SHARED.SKU` AS ITM
                     ON B.SKU_NBR = ITM.SKU_NBR 
                        AND ITM.ACTV_FLG = TRUE
          INNER JOIN (SELECT * 
                        FROM `pr-edw-views-thd.SHARED.CAL_PRD_HIER`
                       WHERE FSCL_WK.FSCL_WK_END_DT = CAL_DT)      A2
                       ON B.CAL_DT = A2.CAL_DT        
          LEFT JOIN (SELECT* 
                       FROM (SELECT SO_SKU_NBR, 
                                    OMS_ITEM_ID, 
                                    ROW_NUMBER() OVER (PARTITION BY SO_SKU_NBR ORDER BY THD_ITEM_STAT_CD ASC,THD_ONLN_FLG DESC,OMS_ITEM_ID DESC) AS RNK
                               FROM `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL` 
                              WHERE ACTV_FLG = 'Y'
                             ) A WHERE  RNK = 1
                    )AS   C
                    ON A.ITEM_ID = C.SO_SKU_NBR
          LEFT JOIN (SELECT CAL_DT, 
                            OMS_ITEM_ID, 
                            CURR_COST_AMT 
                       FROM `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL_DLY` 
                      WHERE (CAL_DT > DATE_SUB(CURRENT_DATE , INTERVAL  91  DAY) 
                             OR CAL_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  456 DAY) 
                             AND DATE_SUB(CURRENT_DATE, INTERVAL  360 DAY) )
                    ) AS D
                    ON C.OMS_ITEM_ID = D.OMS_ITEM_ID
                       AND A2.CAL_DT = D.CAL_DT
    
          LEFT JOIN (SELECT *
                       FROM(SELECT CAL_DT
                                   ,SKU_NBR
                                   ,STR_NBR
                                   ,SKU_STAT_CD
                                   ,A.ACTV_FLG
                                   ,MVNDR_NBR
                                   ,CURR_COST_AMT
                                   ,A.EFF_BGN_DT
                                   ,A.EFF_END_DT
                                   ,ROW_NUMBER() OVER (PARTITION BY SKU_NBR, STR_NBR, CAL_DT ORDER BY SKU_STAT_CD ASC,MVNDR_NBR DESC) AS RNK
                              FROM `pr-edw-views-thd.SHARED.MVNDR_SKU_STR` A 
                                   INNER JOIN `pr-edw-views-thd.SHARED.CAL_PRD_HIER` C
                                              ON C.CAL_DT >= A.EFF_BGN_DT
                                                 AND C.CAL_DT < A.EFF_END_DT
                             WHERE A.STR_NBR = '8119' --8119
                                   AND C.CAL_DT > DATE_SUB(CURRENT_DATE, INTERVAL  456 DAY) 
                                   AND C.CAL_DT < CURRENT_DATE
                                   AND C.CAL_DT = C.FSCL_WK.FSCL_WK_END_DT
                          ) A 
     
                     WHERE RNK = 1
                    )  N
                     ON B.SKU_NBR = N.SKU_NBR
                        AND B.CAL_DT = N.CAL_DT
          INNER JOIN (SELECT * 
                        FROM `pr-edw-views-thd.SHARED_BASE_VNDR.DC_DSVC_TYP`
                       WHERE ACTV_FLG = TRUE 
                             AND DSVC_TYP_CD = 15)    A3
                     ON B.DC_NBR = A3.DC_NBR
inner JOIN(
SELECT * FROM (SELECT
X4.*
,RANK()OVER (PARTITION BY SKU_NBR ORDER BY SKU.SKU_STAT_CD DESC, SKU.SKU_STAT_DESC ASC, LAST_UPD_TS DESC) AS RNK
FROM   `pr-edw-views-thd.SHARED.SKU_HIER`X4
) A
WHERE RNK = 1
) e
                     ON B.SKU_NBR = E.SKU_NBR
          LEFT  JOIN (SELECT * 
                        FROM `pr-edw-views-thd.SHARED.MVNDR` 
                       WHERE ACTV_FLG = TRUE)    G
                      ON N.MVNDR_NBR = G.MVNDR_NBR
          LEFT  JOIN (SELECT * 
                        FROM `pr-supply-chain-thd.PRODUCT.N_ITEM_VLCTY` 
                       WHERE LANG_CD = 'en_US' )    V
                      ON A.ITEM_VLCTY_CD = V.ITEM_VLCTY_CD 
          LEFT  JOIN (SELECT SKU_NBR, 
                             MIN(RCVD_DT) as FIRST_RECPT_DT, 
                             MAX(RCVD_DT) as LAST_RECPT_DT 
                       FROM `pr-edw-views-thd.SCHN_ACTVY.DC_RECPT_SKU`
                      GROUP BY SKU_NBR)    F
                      ON  B.SKU_NBR  = F.SKU_NBR
          LEFT JOIN (SELECT * 
                       FROM `pr-edw-views-thd.SHARED.MVNDR`
                      WHERE ACTV_FLG = TRUE)    H
                     ON N.MVNDR_NBR = H.MVNDR_NBR
      WHERE (A.FSCL_WK_END_DT > DATE_SUB(CURRENT_DATE, INTERVAL  91 DAY)  
             OR A.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  456 DAY)  AND DATE_SUB(CURRENT_DATE, INTERVAL  360 DAY) )
            AND OH_QTY > 0 
AND CASE 
                 WHEN N.SKU_STAT_CD IN (400,600) THEN 'I'
                 WHEN N.SKU_STAT_CD IN (500) THEN 'C'
                 WHEN N.SKU_STAT_CD IN (100,200) AND A.ITEM_VLCTY_CD = 5 THEN 'E'
                 WHEN N.SKU_STAT_CD IN (100,200) AND A.ITEM_VLCTY_CD <> 5 THEN 'Active Overstock'
                 ELSE 'NOT ICE' 
             END IN ('I','C','E',  'Active Overstock')
    GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17
);

/*

CREATE OR REPLACE TABLE `analytics-supplychain-thd.AAA_TEMP.ICE_DASHBOARD_ATTR_22` AS (
SELECT NULL AS CAL_DT --A2.FSCL_WK.FSCL_WK_END_DT AS CAL_DT 
    ,NULL AS YOW_ITEM_SKU_NBR --, ITM.SKU_NBR AS YOW_ITEM_SKU_NBR
    ,NULL AS YOW_ITEM_DESC --, ITM.SKU_DESC AS YOW_ITEM_DESC
    ,NULL AS MVNDR_NBR --, G.MVNDR_NBR
,NULL AS MVNDR_NM    --, G.MVNDR_NM
,NULL AS FIRST_RECPT_DT    --, F.FIRST_RECPT_DT
,NULL AS LAST_RECPT_DT    --, F.LAST_RECPT_DT
,NULL AS REL_THD_ITEM_ID    --, A.ITEM_ID AS REL_THD_ITEM_ID
,NULL AS ITEM_GRP_CD    --, E.ITEM_GRP_CD
,NULL AS ITEM_GRP_DESC    --, E.ITEM_GRP_DESC
,NULL AS ITEM_CLASS_CD    --, E.ITEM_CLASS_CD
,NULL AS ITEM_CLASS_DESC    --, E.ITEM_CLASS_DESC
,NULL AS ITEM_SC_CD    --, E.ITEM_SC_CD
,NULL AS ITEM_SC_DESC    --, E.ITEM_SC_DESC
,NULL AS ITEM_STAT_CD    --, N.ITEM_STAT_CD
,NULL AS ITEM_VLCTY_DESC    --, ITEM_VLCTY_DESC 
,NULL AS ICE_CD     
  
-- CASE   WHEN N.SKU_STAT_CD IN (400,600) THEN 'I'   WHEN N.SKU_STAT_CD IN (500) THEN 'C'
    --    WHEN N.SKU_STAT_CD IN (100,200) AND A.ITEM_VLCTY_CD = 5 THEN 'E'
    --    WHEN N.SKU_STAT_CD IN (100,200) AND A.ITEM_VLCTY_CD <> 5 THEN 'Active Overstock'
    --    ELSE 'NOT ICE' END AS ICE_CD
    --, NULL AS SKU_DC_CNT
    --, SUM(B."OH UNITS")  AS OH_QTY
    --, SUM(B."OH UNITS" * D.CURR_COST_AMT) AS  OH_COST_AMT
,NULL AS SKU_DC_CNT   
,NULL AS OH_QTY    
,NULL AS OH_COST_AMT  
    , NULL AS  OH_RETL_AMT
    ,1 AS PPS_FLG

FROM `pr-edw-views-thd.SHARED.SKU`

FROM LAB_IPR.ICE_DASHBOARD_PPS_LIST B
     INNER JOIN `pr-edw-views-thd.SHARED.SKU` AS ITM
                 ON B.SO_SKU_NBR = ITM.SKU_NBR 
                    AND ITM.ACTV_FLG = TRUE
     INNER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED.CAL_PRD_HIER` WHERE FSCL_WK.FSCL_WK_END_DT = CAL_DT) A2
ON B.CAL_DT BETWEEN A2.FSCL_WK.FSCL_WK_END_DT AND A2.FSCL_WK.FSCL_WK_END_DT

LEFT JOIN `pr-edw-views-thd.SCHN_INV.YOW_ITEM_VLCTY`  AS A
    ON A.ITEM_ID = ITM.SKU_NBR
    AND A.FSCL_WK_END_DT = A2.CAL_DT    
   
LEFT OUTER JOIN (
                 SELECT* 
                   FROM(SELECT SO_SKU_NBR, 
                               OMS_ITEM_ID, 
                               ROW_NUMBER() OVER (PARTITION BY SO_SKU_NBR ORDER BY THD_ITEM_STAT_CD ASC, THD_ONLN_FLG DESC, OMS_ITEM_ID DESC) AS RNK
                   FROM `uat-supply-chain-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL`
                   WHERE ACTV_FLG = 'Y') A 
              WHERE  RNK = 1
               )AS C
    ON A.ITEM_ID = C.SO_SKU_NBR
LEFT OUTER JOIN (SELECT *
                   FROM `uat-supply-chain-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL_DLY` 
                  WHERE ((CAL_DT > DATE_SUB(CURRENT_DATE, INTERVAL 91 DAY)) 
                        OR 
                         (CAL_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL -456 DAY)  
                                 AND DATE_SUB(CURRENT_DATE, INTERVAL 360 DAY)
                         )))

 AS D
    ON C.OMSID = D.OMS_ITEM_ID
    AND A2.CAL_DT = D.CAL_DT
    
    
    LEFT OUTER JOIN (
SELECT*
    FROM(
            SELECT CAL_DT
                  ,SKU_NBR
                  ,STR_NBR
                  ,SKU_STAT_CD
                  ,A.ACTV_FLG
                  ,MVNDR_NBR
                  , CURR_COST_AMT
                  ,A.EFF_BGN_DT
                  ,A.EFF_END_DT
                  ,ROW_NUMBER() OVER (PARTITION BY SKU_NBR, STR_NBR, CAL_DT ORDER BY SKU_STAT_CD ASC, MVNDR_NBR DESC) AS RNK
             FROM `pr-edw-views-thd.SHARED.MVNDR_SKU_STR` A 
                   INNER JOIN `pr-edw-views-thd.SHARED.CAL_PRD_HIER` C
                              ON C.CAL_DT >= A.EFF_BGN_DT
                                 AND C.CAL_DT < A.EFF_END_DT
            WHERE A.STR_NBR = 2527 --8119
                  AND C.CAL_DT > DATE - 456
                  AND C.CAL_DT < DATE
                  AND C.CAL_DT = C.FSCL_WK.FSCL_WK_END_DT
     ) A 
WHERE RNK = 1 ) N
    ON ITM.SKU_NBR = N.SKU_NBR
    AND A2.CAL_DT = N.CAL_DT
    
    -------NEED TO ADD THIS BACK
 INNER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED_BASE_VNDR.DC_DSVC_TYP` WHERE ACTV_FLG = 'Y' AND DSVC_TYP_CD = 15) A3
      ON B.DC_LOC_ID = A3.DC_LOC_ID
  
INNER JOIN `pr-edw-views-thd.SHARED.SKU_HIER` E
           ON ITM.SKU_NBR = E.SKU_NBR
LEFT OUTER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED.MVNDR` WHERE ACTV_FLG = TRUE) G
                ON N.MVNDR_NBR = G.MVNDR_NBR
LEFT OUTER JOIN (SELECT * FROM `pr-supply-chain-thd.PRODUCT.N_ITEM_VLCTY` WHERE LANG_CD='en_US' ) V
                ON A.ITEM_VLCTY_CD = V.ITEM_VLCTY_CD 
LEFT OUTER JOIN (
    SELECT SKU_NBR , 
           MIN(RCVD_DT) as FIRST_RECPT_DT, 
           MAX(RCVD_DT) as LAST_RECPT_DT 
      FROM `pr-edw-views-thd.SCHN_ACTVY.DC_RECPT_SKU` 
    GROUP BY SKU_NBR) F
    ON A.ITEM_ID  = F.SKU_NBR
LEFT OUTER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED.MVNDR` WHERE ACTV_FLG = TRUE) H
    ON N.MVNDR_NBR = H.MVNDR_NBR 
       AND "OH UNITS" > 0 
--WHERE (A.FSCL_WK_END_DT > DATE - 91 OR A.FSCL_WK_END_DT BETWEEN DATE - 456 AND DATE - 360)
--AND ICE_CD IN ('I','C','E')
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17

--LIMIT 1 
);


CREATE OR REPLACE TABLE `analytics-supplychain-thd.AAA_TEMP.ICE_DASHBOARD_ACTV_OVERSTOCK_SEASONAL` AS (
SELECT C.DEPT_NBR,
       B.CLASS_NBR
  FROM `pr-edw-views-thd.SHARED_BASE_PRODUCT.INIT_T1_SNLTY_ICLS` A
       INNER JOIN `pr-edw-views-thd.SHARED.CLASS_HIER` B
                  ON A.CLASS_NBR = B.CLASS.EXT_CLASS_NBR	
       INNER JOIN `pr-supply-chain-thd.CODE_TABLES.ITEM_DEPT` C
                  ON CAST(B.DEPT_NBR AS STRING) = C.ITEM_DEPT_ID
WHERE A.ACTV_FLG = TRUE
AND B.CLASS.CLASS_ACTV_FLG = TRUE
AND C.EFF_END_DT = '9999-12-31'
);

*/

/*

	

----COLLECTION SKU GTIN GLN NUMBER-------------

CREATE TABLE COLLECTION_SKU_GTIN_GLN AS (
  SELECT A.GTIN_NBR
    , A.GLN_NBR
    ,ATTR_NM
    , MAX(CASE WHEN B.ITEM_ATTR_NODE_ID = 1457 THEN ATTR_VAL ELSE NULL END ) AS YOW_SKU_NBR--YOW SKU
    , MAX(CASE WHEN B.ITEM_ATTR_NODE_ID = 892 THEN ATTR_VAL ELSE NULL END ) AS BROWSE_STATUS--THD Browse Onlinestatus
    , MAX(CASE WHEN B.ITEM_ATTR_NODE_ID = 1420 THEN ATTR_VAL ELSE NULL END ) AS DOTCOM_STATUS--THD Dot Com Onlinestatus
    , MAX(CASE WHEN B.ITEM_ATTR_NODE_ID = 3713 THEN ATTR_VAL ELSE NULL END ) AS ONLINE_STATUS--THD OnlineStatus
    , MAX(CASE WHEN B.ITEM_ATTR_NODE_ID = 8011 THEN ATTR_VAL ELSE NULL END ) AS MKT_401--THDInMarket401
    , MAX(CASE WHEN B.ITEM_ATTR_NODE_ID = 10941 THEN ATTR_VAL ELSE NULL END ) AS DOTCOM_DISCONTINUED--DotCom Discontinued Flag
    , MAX(CASE WHEN B.ITEM_ATTR_NODE_ID = 6073 THEN ATTR_VAL ELSE NULL END ) AS HIDE_SKU_ON_SITE--Hide SKU on Web Site
    , MAX(CASE WHEN B.ITEM_ATTR_NODE_ID = 10842 THEN ATTR_VAL ELSE NULL END ) AS HIDE_SKU_ON_SITE_UNTIL_DATE -- Hid on Site Until Date
    , MAX(CASE WHEN B.ITEM_ATTR_NODE_ID = 2543 THEN ATTR_VAL ELSE NULL END ) AS BOPIS_ELIG_STATUS--BOPIS
    , MAX(CASE WHEN B.ITEM_ATTR_NODE_ID = 11078 THEN ATTR_VAL ELSE NULL END ) AS BOSS_ELIG_STATUS--BOSS
    , MAX(CASE WHEN B.ITEM_ATTR_NODE_ID = 8593 THEN ATTR_VAL ELSE NULL END ) AS YOW_CHUB -- YOW CHUB
    , MAX(CASE WHEN B.ITEM_ATTR_NODE_ID = 9382 THEN ATTR_VAL ELSE NULL END ) AS BACKORDER_ELIG_STATUS -- Backorder Eligible
    FROM PR_US_MASTER_VIEWS.GTIN_GCNTRY_TXATTR A
    INNER JOIN PR_US_MASTER_VIEWS.IDM_ITEM_ATTR B
        ON A.ITEM_ATTR_NODE_ID =  B.ITEM_ATTR_NODE_ID
    WHERE A.CNTRY_CD = 223
    and ATTR_NM LIKE '%COLLECTION%'
    GROUP BY 1,2 ,3
) WITH DATA;

*/


CREATE OR REPLACE TABLE `analytics-supplychain-thd.AAA_TEMP.ice_dashboard_last_sold`
AS (
SELECT *
  FROM
      (SELECT A.OMS_ITEM_ID, 
              SO_SKU_NBR AS YOW_SKU_NBR 
              ,ITEM_LAST_SOLD_DT
              ,RANK() OVER (PARTITION BY A.CAL_DT, SO_SKU_NBR ORDER BY R8W_AVG_FCST_SLS_QTY DESC, A.OMS_ITEM_ID DESC) AS RNK
     --       ,R8W_AVG_FCST_SLS_QTY
              ,CASE WHEN SHR_ITEM_FLG = 'Y' THEN 'Y' ELSE 'N' END AS SHARED_FLAG
              ,CASE WHEN ITEM_SHP_FR_VAL = 'CHUB_OMS' THEN 'Factory'
                    WHEN ITEM_SHP_FR_VAL = 'YOW' THEN 'House' 
                    ELSE 'Undefined' 
                END AS ShipMethod
              ,ORDRBL_STR_CNT AS ACTV_STR_CNT , BOD_RSRV_QTY as RSRV_QTY
              ,A.CAL_DT
         FROM `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL_DLY` A
      INNER JOIN `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL` B
                  ON A.DIR_FULFL_ITEM_DTL_ID = B.DIR_FULFL_ITEM_DTL_ID
      INNER JOIN `pr-edw-views-thd.SCHN_SHARED.DFDRL_ITEM_SHP_FR_DIM` X2
                  ON CAST(A.DFDRL_ITEM_SHP_FR_DIM_ID AS STRING) = X2.DFDRL_ITEM_SHP_FR_DIM_ID
      INNER JOIN `pr-edw-views-thd.SHARED.CAL_PRD_HIER` T
                  ON A.CAL_DT = T.CAL_DT
 WHERE 
 (CAST(A.CAL_DT AS DATE) BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 14 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL 0 DAY))
-- (CAST(A.CAL_DT AS DATE) BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 525 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL 365 DAY)
--    OR CAST(A.CAL_DT AS DATE) BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 147 DAY) AND CURRENT_DATE)
              AND A.CAL_DT = T.FSCL_WK.FSCL_WK_END_DT
    AND SO_SKU_NBR IS NOT NULL
) A
WHERE RNK = 1
);

---
CREATE OR REPLACE TABLE `analytics-supplychain-thd.AAA_TEMP.ICE_DASHBOARD_SALES_WLY_new` as (
SELECT COALESCE(STR_SKU_NBR, STR_SKU_NBR) AS YOW_SKU_NBR                                																						
      , FSCL_YR_WK_KEY_VAL																						
       ,FSCL_WK_END_DT              																						
       ,COALESCE(DMND_QTY * C_ITEM_QTY,DMND_QTY) AS DMND_QTY                                																						
  /*    ,COALESCE(R1*C_ITEM_QTY, R1) AS R1               																						
       ,COALESCE(R2*C_ITEM_QTY, R2) AS R2                                																						
       ,COALESCE(R3*C_ITEM_QTY, R3) AS R3                                																						
       ,COALESCE(R4*C_ITEM_QTY, R4) AS R4                                																						
       ,COALESCE(R5*C_ITEM_QTY, R5) AS R5                                																						
       ,COALESCE(R6*C_ITEM_QTY, R6) AS R6                                																						
       ,COALESCE(R7*C_ITEM_QTY, R7) AS R7                                																						
       ,COALESCE(R8*C_ITEM_QTY, R8) AS R8                                																						
       ,COALESCE(R9*C_ITEM_QTY, R9) AS R9                                																						
       ,COALESCE(R10*C_ITEM_QTY, R10) AS R10                                																						
       ,COALESCE(R11*C_ITEM_QTY, R11) AS R11                                																						
       ,COALESCE(R12*C_ITEM_QTY, R12) AS R12                                																						
       ,COALESCE(R13*C_ITEM_QTY, R13) AS R13                                																						
       ,COALESCE(R14*C_ITEM_QTY, R14) AS R14                                																						
       ,COALESCE(R15*C_ITEM_QTY, R15) AS R15                                																						
       ,COALESCE(R16*C_ITEM_QTY, R16) AS R16                                																						
       ,COALESCE(R17*C_ITEM_QTY, R17) AS R17                                																						
       ,COALESCE(R18*C_ITEM_QTY, R18) AS R18                                																						
       ,COALESCE(R19*C_ITEM_QTY, R19) AS R19                                																						
       ,COALESCE(R20*C_ITEM_QTY, R20) AS R20                                																						
       ,COALESCE(R21*C_ITEM_QTY, R21) AS R21                                																						
       ,COALESCE(R22*C_ITEM_QTY, R22) AS R22                                																						
       ,COALESCE(R23*C_ITEM_QTY, R23) AS R23                                																						
       ,COALESCE(R24*C_ITEM_QTY, R24) AS R24                                																						
       ,COALESCE(R25*C_ITEM_QTY, R25) AS R25                                																						
       ,COALESCE(R26*C_ITEM_QTY, R26) AS R26                                																						
       ,COALESCE(R27*C_ITEM_QTY, R27) AS R27                                																						
       ,COALESCE(R28*C_ITEM_QTY, R28) AS R28                                																						
       ,COALESCE(R29*C_ITEM_QTY, R29) AS R29                                																						
       ,COALESCE(R30*C_ITEM_QTY, R30) AS R30                                                          																						
       ,COALESCE(R53*C_ITEM_QTY, R53) AS R53                                																						
       ,COALESCE(R54*C_ITEM_QTY, R54) AS R54                                																						
       ,COALESCE(R55*C_ITEM_QTY, R55) AS R55                                																						
       ,COALESCE(R56*C_ITEM_QTY, R56) AS R56                                																						
       ,COALESCE(R57*C_ITEM_QTY, R57) AS R57                                																						
       ,COALESCE(R58*C_ITEM_QTY, R58) AS R58                                																						
       ,COALESCE(R59*C_ITEM_QTY, R59) AS R59                                																						
       ,COALESCE(R60*C_ITEM_QTY, R60) AS R60                                																						
       ,COALESCE(R61*C_ITEM_QTY, R61) AS R61                                																						
       ,COALESCE(R62*C_ITEM_QTY, R62) AS R62                                																						
       ,COALESCE(R63*C_ITEM_QTY, R63) AS R63                                																						
       ,COALESCE(R64*C_ITEM_QTY, R64) AS R64                                																						
       ,COALESCE(R65*C_ITEM_QTY, R65) AS R65                                																						
       ,COALESCE(R66*C_ITEM_QTY, R66) AS R66                                																						
       ,COALESCE(R67*C_ITEM_QTY, R67) AS R67                                																						
       ,COALESCE(R68*C_ITEM_QTY, R68) AS R68                                																						
       ,COALESCE(R69*C_ITEM_QTY, R69) AS R69                                																						
       ,COALESCE(R70*C_ITEM_QTY, R70) AS R70                                																						
       ,COALESCE(R71*C_ITEM_QTY, R71) AS R71                                																						
       ,COALESCE(R72*C_ITEM_QTY, R72) AS R72                                																						
       ,COALESCE(R73*C_ITEM_QTY, R73) AS R73                                																						
       ,COALESCE(R74*C_ITEM_QTY, R74) AS R74                                																						
       ,COALESCE(R75*C_ITEM_QTY, R75) AS R75                                																						
       ,COALESCE(R76*C_ITEM_QTY, R76) AS R76                                																						
       ,COALESCE(R77*C_ITEM_QTY, R77) AS R77                                																						
       ,COALESCE(R78*C_ITEM_QTY, R78) AS R78                                																						
       ,COALESCE(R79*C_ITEM_QTY, R79) AS R79                                																						
       ,COALESCE(R80*C_ITEM_QTY, R80) AS R80                                																						
       ,COALESCE(R81*C_ITEM_QTY, R81) AS R81                                																						
       ,COALESCE(R82*C_ITEM_QTY, R82) AS R82    */                        																						
FROM (SELECT   fscl_wk.FSCL_YR_WK_KEY_VAL, 																						
               fscl_wk.FSCL_WK_END_DT                                																						
               ,A.SKU_NBR AS STR_SKU_NBR                                																						
               ,SUM(A.UNT_SLS) AS DMND_QTY                                																						
        /*       ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  7 DAY)  AND CURRENT_DATE                             																						
                     THEN SUM(A.UNT_SLS) ELSE NULL END AS R1                                																						
               ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  13 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  7 DAY)                               																						
                     THEN SUM(A.UNT_SLS) ELSE NULL END AS R2                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  20 DAY)  AND DATE_SUB(CURRENT_DATE, INTERVAL  14 DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R3                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  27 DAY)  AND DATE_SUB(CURRENT_DATE, INTERVAL  21 DAY)                                 																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R4                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  34 DAY)  AND DATE_SUB(CURRENT_DATE, INTERVAL  28  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R5                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  41 DAY)  AND DATE_SUB(CURRENT_DATE, INTERVAL  35   DAY)                               																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R6                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  48 DAY)  AND DATE_SUB(CURRENT_DATE, INTERVAL  42  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R7                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  55 DAY)  AND DATE_SUB(CURRENT_DATE, INTERVAL  49   DAY)                               																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R8                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  62 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  56  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R9                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  69 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  63   DAY)                               																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R10                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  76 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  70  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R11                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  83 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  77    DAY)                              																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R12                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  90 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  84  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R13                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  97 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  91  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R14                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  104 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  98  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R15                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  111 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  105 DAY)                                 																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R16                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  118 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  112  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R17                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  125 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  119  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R18                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  132 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  126  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R19                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  139 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  133  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R20                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  146 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  140  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R21                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  153 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  147  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R22                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  160 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  154  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R23                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  167 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  161  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R24                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  174 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  168  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R25                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  181 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  175  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R26                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  188 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  182  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R27                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  195 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  189   DAY)                               																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R28                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  729 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  196  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R29                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  209 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  203   DAY)                               																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R30                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  370 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  364  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R53                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  377 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  371  DAY)                                																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R54                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  384 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  378   DAY)                               																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R55                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  391 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  385   DAY)                               																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R56                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  398 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  392   DAY)                               																						
                      THEN SUM(A.UNT_SLS) ELSE NULL END AS R57                                																						
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  405 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  399    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R58                                													  									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  412 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  406    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R59                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  419 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  413    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R60                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  426 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  420    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R61                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  433 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  427    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R62                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  440 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  434    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R63                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  447 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  441    DAY)                           																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R64                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  454 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  448    DAY)                      																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R65                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  461 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  455    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R66                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  468 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  462    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R67                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  475 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  469    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R68                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  482 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  476    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R69                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  489 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  483    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R70                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  496 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  490    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R71                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  503 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  497    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R72                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  510 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  504    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R73                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  517 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  511    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R74                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  524 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  518    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R75                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  531 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  525    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R76                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  538 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  532    DAY)               																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R77                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  545 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  539    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R78                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  552 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  546    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R79                                													 									
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  559 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  553    DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R80                                														 								
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  566 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  560    DAY)                               																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R81                                														 								
                ,CASE WHEN FSCL_WK.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  573 DAY) AND  DATE_SUB(CURRENT_DATE, INTERVAL  567          DAY)                       																						
                THEN SUM(A.UNT_SLS) ELSE NULL END AS R82  */
               
         --  FROM `pr-edw-views-thd.TD_COPIES.STITEM_SLS_D` AS A																						
         from `pr-edw-views-thd.SLS.SKU_STR_DAY_SLS` A																						
         --       LEFT JOIN `pr-edw-views-thd.TD_COPIES.ITEM_HIER_MAPPING` ITEM																						
                --             ON A.SKU_NBR = ITEM.ITEM_SKU_NBR																						
                INNER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED.SKU` WHERE ACTV_FLG = TRUE) AS C																						
                           ON A.SKU_NBR = C.SKU_NBR																						
           --     LEFT JOIN  `pr-edw-views-thd.TD_COPIES.LOC_HIER_MAPPING` LOC																						
                       --       ON A.LOC_ID = LOC.LOC_ID             																						
                INNER JOIN (SELECT LOC_NBR, null as LOC_ID FROM `pr-edw-views-thd.SHARED.LOC_HIER` WHERE LOC_NBR = '8119') AS B																						
                           ON A.STR_NBR = B.LOC_NBR																						
                INNER JOIN `pr-edw-views-thd.SHARED.CAL_PRD_HIER` T																						
                           ON SLS_DT = CAL_DT																						
WHERE ( fscl_wk.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 573 DAY) AND DATE_SUB(CURRENT_DATE, INTERVAL 364 DAY)) 																						
                OR fscl_wk.FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL  209 DAY) AND CURRENT_DATE																						
        GROUP BY 1,2,3																						
        ) A LEFT OUTER JOIN                                																						
            (SELECT CLT.SKU_NBR AS COMBO_ITEM_ID, 																						
                    CLT_CPNT.CPNT_SKU_NBR, 																						
                    CLT_CPNT.CPNT_SKU_QTY AS C_ITEM_QTY, 																						
                    CLT.SKU_NBR                                																						
            FROM (SELECT * FROM `pr-edw-views-thd.SHARED_BASE_PRODUCT.SKU_CLT` WHERE ACTV_FLG = TRUE) AS CLT                                                     INNER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED_BASE_PRODUCT.SKU_CLT_CPNT` WHERE ACTV_FLG = TRUE) AS CLT_CPNT 																						
                            ON CLT.SKU_CLT_ID = CAST(CLT_CPNT.SKU_CLT_ID AS INT64)                                																						
                INNER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED.SKU` WHERE ACTV_FLG = TRUE) B                            																						
                            ON CLT.SKU_NBR = B.SKU_NBR  ) B                            																						
    ON A.STR_SKU_NBR = B.SKU_NBR     																						

);   



--`pr-supply-chain-thd.DIRECT_FULFILLMENT_SOURCE.DIR_FULFL_DMND_FCST`
--`pr-supply-chain-thd.DIRECT_FULFILLMENT.DIR_FULFL_DMND_FCST`
--`pr-supply-chain-thd.DIRECT_FULFILLMENT_STAGE.DIR_FULFL_DMND_FCST_STG`

CREATE OR REPLACE TABLE `analytics-supplychain-thd.AAA_TEMP.ICE_DASHBOARD_SALES_WLY_new_2`
AS (
SELECT CLT.SKU_NBR AS COMBO_ITEM_ID, 
            CLT_CPNT.CPNT_SKU_NBR, 
            CLT_CPNT.CPNT_SKU_QTY AS C_ITEM_QTY, 
            CLT.SKU_NBR                                
    FROM (SELECT * FROM `pr-edw-views-thd.SHARED_BASE_PRODUCT.SKU_CLT` WHERE ACTV_FLG = TRUE) AS CLT
        INNER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED_BASE_PRODUCT.SKU_CLT_CPNT` WHERE ACTV_FLG = TRUE) AS CLT_CPNT 
                    ON CLT.SKU_CLT_ID = CAST(CLT_CPNT.SKU_CLT_ID AS INT64)                                
        INNER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED.SKU` WHERE ACTV_FLG = TRUE) B                            
                    ON CLT.SKU_NBR = B.SKU_NBR   
);


CREATE OR REPLACE TABLE `analytics-supplychain-thd.AAA_TEMP.ICE_DASHBOARD_R8W_AVG_FCST_SLS_QTY`
AS (
SELECT FSCL_WK_END_DT
       ,B.SKU_NBR
       ,SUM(FCST_GSLS_QTY) / 8 AS FCST_GSLS_QTY
--FROM `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_DMND_FCST` A
  FROM `pr-edw-views-thd.SCHN_DFC.DIR_FULFL_REPL_FCST` A
      --`pr-supply-chain-thd.DIRECT_FULFILLMENT_SOURCE.DIR_FULFL_REPL_FCST`  -- ONLY HAS 2/11/2019 Data
        INNER JOIN (SELECT * 
                      FROM `pr-edw-views-thd.SHARED.SKU` 
                     WHERE ACTV_FLG = TRUE 
                           AND SKU_STAT_CD = 100) B
                    ON A.SKU_NBR = B.SKU_NBR
        INNER JOIN (SELECT DISTINCT FSCL_WK.FSCL_WK_END_DT, 
                                    FSCL_WK.FSCL_YR_WK_KEY_VAL 
                      FROM `pr-edw-views-thd.SHARED.CAL_PRD_HIER`) T
                   ON A.FCST_CRT_DT = DATE_SUB(T.FSCL_WK_END_DT, INTERVAL 6 DAY)
                   
WHERE FCST_WK_END_DT BETWEEN A.FCST_CRT_DT AND DATE_ADD(A.FCST_CRT_DT, INTERVAL  56 DAY)
      AND (FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 531 DAY) AND DATE_SUB(CURRENT_DATE, INTERVAL 364 DAY)
        OR FSCL_WK_END_DT BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 161 DAY) AND CURRENT_DATE)
GROUP BY 1,2
);

