--Please Add ImportDomesticIndicator and ExpectedAllocationQty and DSVCTypeCode to the last query
-- Author:		PRASHANT PARIKH
-- Create date: 3/10/2019

CREATE OR REPLACE TABLE `analytics-supplychain-thd.DF_IPR_BI.DF_ANALYTICS_DB_DFC_SKU_INV_REVIEWDAYS` AS (
  SELECT LOCATIONID, 
         VENDORNUMBER, 
         REVIEWDAYS 
    FROM `pr-edw-views-thd.SCHN_FCST.REVIEWSCHEDULE`
   WHERE CAST(locationid AS INT64) > 05937
         AND CAST(locationid AS INT64) < 06778
);	
	
CREATE OR REPLACE TABLE `analytics-supplychain-thd.DF_IPR_BI.DF_ANALYTICS_DB_DFC_SKU_INV_MVNDR_ORG` AS (
SELECT DISTINCT MVNDR_NBR,
                MVNDR_NM
           FROM `pr-edw-views-thd.SHARED.MVNDR` 
          WHERE ACTV_FLG = true
);	 
	  
--SELECT * FROM `pr-supply-chain-thd.DCM.MVNDR_SKU_DFC_HIST`
  
  
  
CREATE OR REPLACE TABLE `analytics-supplychain-thd.DF_IPR_BI.DF_ANALYTICS_DB_DFC_BYP_SKU_INV` AS (
SELECT *	
FROM	
(	
 SELECT 
  DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY) CAL_DT 
, CURRENT_TIMESTAMP AS UPDATED_TS
,D.SUB_DEPT.SUB_DEPT_NBR as SubDept --as ""
, D.CLASS.CLASS_NBR Class --AS ""
, D.CLASS.CLASS_DESC ClassDesc --AS "ClassDesc"
, D.SUB_CLASS.SUB_CLASS_NBR as Subclass
, D.SUB_CLASS.SUB_CLASS_DESC --as "SubclassDesc"

, E2.OMS_ID AS OMSID
, A.SKU_NBR SKUNumber --AS "SKUNumber"
, D.SKU.SKU_DESC SKUDesc --AS "SKUDesc"
, H.SKU_STAT_CD  MKT401SKUStatus  -- AS  "MKT401SKUStatus"
--, A.DC_LOC_ID 
, CAST(A.DC_NBR AS INT64) DCNumber -- AS "DCNumber"
, case when cast(A.DC_NBR as INT64) = 6705 then 'YOW - LOCUST GROVE LT BK' else LOC.LOC_NM end as DCName --AS "DCName"  This is located in table C
, A.SKU_STAT_CD DCSKUStatus -- AS "DCSKUStatus"
,CURR_DSVC_TYP_CD
--, PRIM_MVNDR_PRTY_ID
, E.MVNDR_NBR MVNDRNumber --AS "MVNDRNumber"
,L.ALT_MVNDR_NBR ALTMVNDRNumber --AS "ALTMVNDRNumber"
--,M.ALT_MVNDR_NM AS "ALT MVNDR Name"
, E.MVNDR_NM MVNDRName-- AS "MVNDRName"
,NULL AS ImportDomesticInd --, CASE WHEN CAPTN_BYO_NBR = 50 THEN 'I' ELSE 'D' END ImportDomesticInd --AS ""
, A3.MVNDR_PART_NBR MFGPartNbr -- AS ""
,A3.BUY_UOM_QTY AS Buypack_Qty
--, CURR_DSVC_TYP_CD AS "DSVC Type Code"
--,CASE WHEN K.THD_SO_SKU_NBR IS NULL THEN (CASE WHEN I.DSVC_TYP_DESC IS NULL THEN 'DFC' ELSE I.DSVC_TYP_DESC END) ELSE 'SDC' AS "DSVC Type Code"
,NULL AS DSVCTypeCode--,COALESCE (K.DSVC_TYPE_CD, I.DSVC_TYP_DESC, 'DFC') DSVCTypeCode --AS "DSVC Type Code"
, A3.CURR_COST_AMT CurrentCostAmount --AS "Current Cost Amount"
, A2.CURR_RETL_AMT CurrentRetailAmount -- AS "Current Retail Amount"
, IFNULL(F.OHQTY,0) OHQty -- AS "OH Qty"
, IFNULL(F.AVAILOHQTY,0) AvailOHQty -- as "Avail OH Qty"
, IFNULL(F.OOQTY,0) OOQty -- as "OO Qty"
--,CONF_ALLOC_QTY ConfirmedAllocationQty -- AS "Confirmed Allocation Qty"    --Revisit
,null as ConfirmedAllocationQty,
null as ExpectedAllocationQty
--,EXPCTD_ALLOC_QTY ExpectedAllocationQty -- AS "Expected Allocation Qty"    --Revisit
,RANK()OVER(PARTITION BY A.SKU_NBR, A.DC_NBR ORDER BY H.SKU_STAT_CD ASC, 
 CASE WHEN E.MVNDR_NM NOT LIKE '%GENERIC%' 
     THEN 1 ELSE 0 
 END DESC , 
 E.MVNDR_NBR DESC) AS RNK
 --,RANK()OVER(PARTITION BY A.ITEM_ID, A.DC_LOC_ID ORDER BY H.ITEM_STAT_CD ASC, CASE WHEN E.MVNDR_NM NOT LIKE '%GENERIC%' THEN 1 ELSE 0 END DESC , E.MVNDR_NBR DESC) AS RNK


FROM (SELECT * FROM `pr-edw-views-thd.SHARED.SKU_DC` WHERE ACTV_FLG = TRUE) A
LEFT OUTER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED.SKU_STR` WHERE ACTV_FLG = TRUE 
AND STR_NBR = '8119' ) A2 --8119 STATUS
    ON A.SKU_NBR = A2.SKU_NBR
	
LEFT OUTER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED.MVNDR_SKU_DC` WHERE ACTV_FLG = TRUE) A3
    ON A.SKU_NBR = A3.SKU_NBR
    AND A.DC_NBR = A3.DC_NBR
    AND A.PRIM_MVNDR_NBR = A3.MVNDR_NBR
INNER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED_BASE_VNDR.DC_DSVC_TYP` WHERE DSVC_TYP_CD = 15) B
    ON A.DC_NBR = B.DC_NBR
LEFT OUTER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED.LOC_HIER` WHERE ACTV_FLG = TRUE 
                 AND CAST(LOC_NBR AS INT64) IN (5938,6007,6777,6707,6760,6705,6006) AND STR_DC_IND = 'DC' ) C
    ON A.DC_NBR = C.LOC_NBR
    
    LEFT OUTER JOIN(
SELECT * FROM (SELECT
X4.*
,RANK()OVER (PARTITION BY SKU_NBR ORDER BY SKU.SKU_STAT_CD DESC, SKU.SKU_STAT_DESC ASC, LAST_UPD_TS DESC) AS RNK
FROM   `pr-edw-views-thd.SHARED.SKU_HIER`X4
) A
WHERE RNK = 1
) D
ON  a.SKU_NBR = D.SKU_NBR
    
LEFT OUTER JOIN (SELECT * FROM `pr-edw-views-thd.SHARED.MVNDR` WHERE ACTV_FLG = TRUE) E
            ON A3.MVNDR_NBR = E.MVNDR_NBR                   
LEFT OUTER JOIN 
(	
--SELECT *	
--FROM(	
--SELECT A.*,RANK()OVER(PARTITION BY OMS_THD_SKU_NBR ORDER BY LAST_UPD_TS DESC, EFF_BGN_DT DESC, DFC_THD_MVNDR_NBR DESC) AS RNK
--  FROM `pr-edw-views-thd.SHARED_BASE_PRODUCT.OMS_ONLN_ITEM` A 
-- WHERE ACTV_FLG = TRUE 
--) A WHERE RNK = 1 

SELECT *
FROM(	
SELECT A.*,RANK()OVER(PARTITION BY OMS_THD_SKU_NBR ORDER BY ACTV_FLG DESC, EFF_END_DT DESC, OMS_COST_AMT DESC, DFC_THD_MVNDR_NBR DESC) AS RNK
  FROM `pr-edw-views-thd.SHARED_BASE_PRODUCT.OMS_ONLN_ITEM` A 
) A WHERE RNK = 1 

)E2
ON A.SKU_NBR = E2.OMS_THD_SKU_NBR

    LEFT OUTER JOIN(
SELECT * FROM (SELECT
X4.*
,RANK()OVER (PARTITION BY SKU_NBR ORDER BY SKU.SKU_STAT_CD DESC, SKU.SKU_STAT_DESC ASC, LAST_UPD_TS DESC) AS RNK
FROM   `pr-edw-views-thd.SHARED.SKU_HIER`X4
) A
WHERE RNK = 1
) e3
ON  a.SKU_NBR = e3.SKU_NBR
    
    
LEFT OUTER JOIN (
    SELECT    NULL AS LOC_ID 
    , B.LOC_NBR AS DFC_LOC_NBR
    , null as LOC_NM
    , SO_SKU_NBR AS ITEM_ID
    , C.SKU_NBR AS SKU_NBR
    , C.SKU.SKU_DESC
    ,CAL_DT	
    ,sum(DFC_OO_QTY) AS OOQTY 
    , SUM(DFC_OH_QTY) OHQTY -- AS "OHQTY"
   -- , SUM(CASE WHEN INV_TYP = 'ALLOCATABLE' OR LOCK_CD IN ('CB', 'PP', 'QA', 'RB', 'PS', 'LCB', 'LPP', 'LQA', 'LRB', 'LPS') THEN QTY ELSE 0 END) AS "AVAIL_OH_QTY"
    ,SUM(DFC_AVAIL_OH_QTY) AS AVAILOHQTY
	FROM `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_BRCH_DLY` A
	INNER JOIN `pr-edw-views-thd.DF_ANALYTICS.DIR_FULFL_ITEM_DTL` X
    ON a.oms_item_id = X.oms_item_id
    AND a.dir_fulfl_item_dtl_id  = X.dir_fulfl_item_dtl_id
	AND X.ACTV_FLG = 'Y'
    LEFT OUTER JOIN (SELECT  *
                       FROM `pr-edw-views-thd.SHARED.LOC_HIER` 
                      WHERE ACTV_FLG = TRUE 
                            AND CAST(LOC_NBR AS INT64) IN (5938,6007,6777,6707,6760,6705,6006) 
                                AND str_dc_ind = 'DC') B
                    ON CAST(A.LOC_NBR AS STRING) = B.LOC_NBR
            LEFT OUTER JOIN(
SELECT * FROM (SELECT
X4.*
,RANK()OVER (PARTITION BY SKU_NBR ORDER BY SKU.SKU_STAT_CD DESC, SKU.SKU_STAT_DESC ASC, LAST_UPD_TS DESC) AS RNK
FROM   `pr-edw-views-thd.SHARED.SKU_HIER`X4
) A
WHERE RNK = 1
) C
ON  X.SO_SKU_NBR = c.SKU_NBR
    
        
        
    WHERE CAL_DT = DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY)  
    GROUP BY 1,2,3,4,5,6,7 --HAVING"OH QTY" <> "AVAIL OH QTY"
    ) F	
ON A.SKU_NBR = F.SKU_NBR
AND A.DC_NBR = F.DFC_LOC_NBR

/*
LEFT OUTER JOIN (
    SELECT B.ITEM_ID
    , A.RECV_LOC_ID
    , SUM(ORD_QTY -  ZEROIFNULL(G.RCVD_UNT_CNT)) OO_QTY
     , MIN(EXPCTD_ARVL_DT) EXPCTD_ARVL_DT
    FROM `pr-edw-views-thd.SCHN_ACTVY.PURCH_ORDER` AS A
    LEFT OUTER JOIN `pr-edw-views-thd.SCHN_ACTVY.PURCH_ORDER_LN` AS B
        ON A.ORD_ID = B.ORD_ID
    LEFT OUTER JOIN  `pr-edw-views-thd.SCHN_SHARED.N_ORD_STAT` AS F
        ON A.ORD_STAT_CD = F.ORD_STAT_CD
    LEFT OUTER JOIN ( 
        SELECT A.ORD_ID, B.RCVD_ITEM_ID, SUM(B.RCVD_EA_QTY) RCVD_EA_QTY, SUM(RCVD_UNT_CNT) RCVD_UNT_CNT
        FROM `pr-edw-views-thd.SCHN_ACTVY.DC_RECPT` A
        INNER JOIN `pr-edw-views-thd.SCHN_ACTVY.DC_RECPT_SKU` B
            ON A.DC_RECPT_ID = B.DC_RECPT_ID
            --AND A.CRT_TS = B.CRT_TS
        GROUP BY 1,2
    ) G        	
        ON A.ORD_ID = G.ORD_ID
        AND B.ITEM_ID = G.RCVD_ITEM_ID
        	
    WHERE A.ORD_STAT_CD = 2
    AND DEL_FLG <> 'Y'
    AND A.ORD_SRC_TYP_CD = 3 -- POM is PO source
    AND A.DSVC_TYP_CD = 15    
    GROUP BY 1,2) G
        ON A.ITEM_ID = G.ITEM_ID
        AND A.DC_LOC_ID = G.RECV_LOC_ID
*/    	
LEFT JOIN        
(SELECT SKU_NBR, SKU_STAT_CD, MVNDR_NBR
FROM `pr-edw-views-thd.SHARED.MVNDR_SKU_STR` 
 WHERE ACTV_FLG = true AND STR_NBR = '8119'
       AND CURR_DSVC_TYP_CD = 15 
 ) H	
ON A3.MVNDR_NBR = H.MVNDR_NBR 
 AND A3.SKU_NBR = H.SKU_NBR
	--  Come back to this later
--LEFT JOIN (
--SELECT DF_MVNDR_NBR, SUBSTR(DFC_LOC_NBR,2,5) AS DFC_LOC_NUMBER, THD_SO_SKU_NBR
--, CASE WHEN DSVC_TYP_CD IN (2,3) THEN 'RDC' WHEN DSVC_TYP_CD IN (4,6) THEN 'SDC' ELSE 'DFC' END AS DSVC_TYP_DESC 
--FROM  LAB_IPR.DFC_SHRD_SKU
--) I	
--ON D.ITEM_SKU_NBR = I.THD_SO_SKU_NBR
--AND E.MVNDR_NBR = I.DF_MVNDR_NBR 
--AND C.LOC_NBR = I.DFC_LOC_NUMBER
	
/*	
LEFT JOIN (
SELECT DF_SKU, 
       A.SKU_NBR, 
       A.LOC_ID, 
       ITEM_SKU_NBR, 
       A.LOC_TYP_CD, 
       LOC_NBR, 
       LOC_NM, 
       SUM(CONF_ALLOC_QTY) AS CONF_ALLOC_QTY, 
       SUM(EXPCTD_ALLOC_QTY) AS EXPCTD_ALLOC_QTY
  FROM `pr-supply-chain-inv-thd.SCDA_SUPPLYCHAIN_SRC.STRSK_VIRT_ALLOC` A
       INNER JOIN (SELECT * 
                     FROM `pr-edw-views-thd.SHARED.LOC_HIER`
                    WHERE ACTV_FLG = 'Y') B
                   ON A.LOC_ID = B.LOC_ID
       INNER JOIN (SELECT * 
                     FROM `pr-edw-views-thd.SHARED_BASE_VNDR.DC_DSVC_TYP` WHERE ACTV_FLG = 'Y' AND DSVC_TYP_CD = 15)  B2
                   ON A.LOC_ID = B2.DC_LOC_ID
       INNER JOIN `pr-edw-views-thd.SHARED.SKU_HIER` C
    ON A.ITEM_ID = C.ITEM_ID
INNER JOIN (SELECT MAX(LOAD_DT) AS LOAD_DT, 
                   MAX(LAST_UPD_TS) AS LAST_UPD_TS, 
                   ITEM_ID, 
                   LOC_ID 
              FROM `pr-supply-chain-inv-thd.SCDA_SUPPLYCHAIN_SRC.STRSK_VIRT_ALLOC` A 
          GROUP BY 3,4
           ) D	
          ON A.ITEM_ID = D.ITEM_ID
             AND A.LOC_ID = D.LOC_ID
             AND A.LOAD_DT = D.LOAD_DT 
             AND A.LAST_UPD_TS = D.LAST_UPD_TS
--INNER JOIN LAB_IPR.SKU_XREF E    --Come back to later
--ON ITEM_SKU_NBR = CORE_SKU
           GROUP BY 1,2,3,4,5,6,7
) J 	
ON C.LOC_NBR = J.LOC_NBR
 on   D.ITEM_SKU_NBR = J.DF_SKU

*/

LEFT JOIN 	
(	
SELECT DISTINCT MVNDR_NBR, ALT_MVNDR_NBR
FROM(	
SELECT MVNDR_NBR, ALT_MVNDR_NBR
,RANK()OVER(PARTITION BY MVNDR_NBR ORDER BY SKU_STAT_CD ASC, MVNDR_SKU_STAT_CD ASC, ALT_SKU_NBR DESC) AS RNK
FROM `pr-supply-chain-thd.DCM.MVNDR_SKU_DFC_HIST` A 
WHERE ALT_MVNDR_NBR IS NOT NULL
AND ACTIVE_FLG = 'Y') A
WHERE RNK = 1
) L	
ON L.MVNDR_NBR = E.MVNDR_NBR

)
WHERE RNK = 1);	 


-------------
-------------
-------------
-- SQL SERVER PART

USE [DF_STAGE]
GO
/****** Object:  StoredProcedure [dbo].[SP_DFC_SKU_INV]    Script Date: 7/4/2019 2:59:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		PRASHANT PARIKH
-- Create date: 3/10/2019
-- Description:	DFC SKU INV
---
-- =============================================
ALTER PROCEDURE [dbo].[SP_DFC_SKU_INV] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	PRINT 'REVIEW DAYS STARTED'

IF OBJECT_ID('DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV_REVIEWDAYS', 'U') IS NOT NULL
	drop table DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV_REVIEWDAYS;
declare @src_qry as varchar(max)
set @src_qry = 

'

  SELECT LOCATIONID, 
         VENDORNUMBER, 
         REVIEWDAYS 
    FROM `pr-edw-views-thd.SCHN_FCST.REVIEWSCHEDULE`
   WHERE CAST(locationid AS INT64) > 05937
         AND CAST(locationid AS INT64) < 06778
' 
exec DF_STAGE.dbo.usp_import_data_from_BigQuery
       @src_qry = @src_qry
,@dest_tbl = 'DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV_REVIEWDAYS'
,@crt_dest_tbl = 1;


IF OBJECT_ID('DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV_MVNDR_ORG', 'U') IS NOT NULL
DROP TABLE  DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV_MVNDR_ORG

--declare @src_qry as varchar(max)
set @src_qry = 
'
SELECT DISTINCT MVNDR_NBR,
                MVNDR_NM
           FROM `pr-edw-views-thd.SHARED.MVNDR` 
' 

exec DF_STAGE.dbo.usp_import_data_from_BigQuery
       @src_qry = @src_qry
,@dest_tbl = 'DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV_MVNDR_ORG'
,@crt_dest_tbl = 1;




PRINT 'DFC BUY SKU INV BQ STARTED'


IF OBJECT_ID('DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_BYP_SKU_INV_BQ', 'U') IS NOT NULL
DROP TABLE  DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_BYP_SKU_INV_BQ

--declare @src_qry as varchar(max)
set @src_qry = 
'
SELECT*
FROM `analytics-supplychain-thd.DF_IPR_BI.DF_ANALYTICS_DB_DFC_BYP_SKU_INV`
  
'
exec DF_STAGE.dbo.usp_import_data_from_BigQuery
       @src_qry = @src_qry
,@dest_tbl = 'DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_BYP_SKU_INV_BQ'
,@crt_dest_tbl = 1;



PRINT 'REFRESH DFC SKU OR NOT?';
declare @Check_OMSID_DTL_TBL as int = 0

declare @DateInOMS_DTL_PREV_DT as date
SELECT @DateInOMS_DTL_PREV_DT = max(cal_dt) from DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV

PRINT @DateInOMS_DTL_PREV_DT


declare @DateInOMS_DTL as date
SELECT @DateInOMS_DTL = max(cal_dt) from DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_BYP_SKU_INV_BQ WITH (NOLOCK)
PRINT @DateInOMS_DTL

declare @OH as float
SELECT @OH = sum(OHQty) from DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_BYP_SKU_INV_BQ WITH (NOLOCK)
PRINT @OH



IF ((@DateInOMS_DTL_PREV_DT < @DateInOMS_DTL OR @DateInOMS_DTL_PREV_DT IS NULL) and @OH >10000)
BEGIN


TRUNCATE TABLE DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_BYP_SKU_INV

PRINT 'DFC BUY SKU INV BQ INSERTION RECORDS STARTED'

INSERT INTO DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_BYP_SKU_INV
SELECT
 [CAL_DT] AS  [CAL_DT]
      ,[UPDATED_TS] AS       [UPDATED_TS]
      ,[SubDept] AS       [Sub Dept]
      ,[Class] AS       [Class]
      ,[ClassDesc] AS   [Class Desc]
      ,[Subclass] AS    [Subclass]
      ,[SUB_CLASS_DESC] AS     [Subclass Desc]
      ,[OMSID] AS       [OMSID]
      ,[SKUNumber] AS     [SKU Number]
      ,[SKUDesc] AS       [SKU Desc]
      ,[MKT401SKUStatus] AS [MKT 401 SKU Status]
      ,[DCNumber] AS       [DC Number]
      ,[DCName] AS       [DC Name]
      ,[DCSKUStatus] AS     [DC SKU Status]
      ,[CURR_DSVC_TYP_CD] AS [CURR_DSVC_TYP_CD]
      ,[MVNDRNumber] AS      [MVNDR Number]
      ,[ALTMVNDRNumber] AS   [ALT MVNDR Number]
      ,[MVNDRName] AS       [MVNDR Name]
      ,[ImportDomesticInd] AS [Import Domestic Ind]
      ,[MFGPartNbr] AS      [MFG Part #]
      ,[Buypack_Qty] AS       [Buypack_Qty]
      ,[DSVCTypeCode] AS       [DSVC Type Code]
      ,[CurrentCostAmount] AS     [Current Cost Amount]
      ,[CurrentRetailAmount] AS     [Current Retail Amount]
      ,[OHQty] AS      [OH Qty]
      ,[AvailOHQty] AS       [Avail OH Qty]
      ,[OOQty] AS       [OO Qty]
      --,[ConfirmedAllocationQty] 
	,conf_alloc_qty  AS   	      [Confirmed Allocation Qty]
     -- ,[ExpectedAllocationQty] 
	, expctd_alloc_qty AS       [Expected Allocation Qty]

FROM DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_BYP_SKU_INV_BQ a
left join (
select oms_thd_sku, dfc_nbr, scnm_dcm_status
, max(conf_alloc_qty) as conf_alloc_qty , max(expctd_alloc_qty)  as expctd_alloc_qty
FROM DFC_VIA_RDC.dbo.dvr_audit
group by oms_thd_sku, dfc_nbr, scnm_dcm_status
) b
on a."skunumber" = b.oms_thd_sku 
and a."dcnumber" = b.dfc_nbr


--- INCORPORATE TOP UP AND SOQ

SELECT MAX(a.FSCL_YR_WK_KEY_VAL) AS MAX_FW
INTO #T1 
FROM DF_STAGE.DBO.TOPUP_COMP1 a WITH (NOLOCK)
inner join (
select distinct fscl_yr_wk_key_val from df_stage.dbo.dim_cal_prd
where cal_dt > getdate()-7 
and cal_dt < getdate()
) b
on a.fscl_yr_wk_key_val = b.fscl_yr_wk_key_val



SELECT A.SKU, A.DC, A.OUTL_RET, A.ROUNDED_SOQ_RET, A.OUTL_QTY, A.ROUNDED_SOQ_QTY, A.FSCL_YR_WK_KEY_VAL
INTO #T2
FROM DF_STAGE.DBO.TOPUP_COMP1 A WITH (NOLOCK)
INNER JOIN #T1 B
ON A.FSCL_YR_WK_KEY_VAL = B.MAX_FW

--------- OO DATA--------


PRINT 'DCM OO STARTED'



IF OBJECT_ID('DF_STAGE.DBO.NKA_OO_REPORT', 'U') IS NOT NULL DROP TABLE DF_STAGE.DBO.NKA_OO_REPORT; 
exec SSISDB.MDDataTech.usp_run_data_transfer
@src_sys = 'NXA1964_DCMprod1', @dest_sys = WAPRCN026B
,@src_qry = '   
SELECT 
    SKU_NBR,
    LOC_NBR,
    DCM_OO,
    ZEROIFNULL(OO)+ZEROIFNULL(CZT_ORD)-ZEROIFNULL(BACK_ORD)+ZEROIFNULL(VIR_ALLOC_OO)   AS TOTAL_OO

FROM 
    (SELECT 
        COALESCE(A.SKU_NBR,B.SKU_NBR,C.SKU_NBR,D.SKU_NBR,E.SKU_NBR) AS SKU_NBR,
            COALESCE(A.LOC_NBR ,B.LOC_NBR ,C.LOC_NBR  ,D.LOC_NBR,E.SKU_NBR) AS LOC_NBR,
        MAX(DCM_OO) AS DCM_OO,
        MAX(OO) AS OO,MAX(CZT_ORD) AS CZT_ORD,MAX(BACK_ORD) AS BACK_ORD,
        MAX(VIR_ALLOC_OO) AS VIR_ALLOC_OO

    FROM
        (SELECT 
            Productnumber AS SKU_NBR,
            LOCATIONID (int) AS LOC_NBR,
            WEEK01 AS DCM_OO
        FROM PR_DCMREPL_VIEWS.FARWEEKLY A
    
        WHERE LOCATIONID IN (6707
                                                ,6705
                                                ,6777
                                                ,6760
                                                ,6006
                                                ,6007
                                                ,5938)
            AND FIELDTYPE=67) A
    FULL OUTER JOIN 
        (SELECT 
            SKU_NBR, 
            LOC_NBR (int),
            SUM(ALLOC_QTY) AS BACK_ORD  
        FROM PR_DCREPL_CSTM_stg.CUST_SKU_DMND_stg
        WHERE LOC_NBR IN (6707
                                                       ,6705
                                                    ,6777
                                                    ,6760
                                                    ,6006
                                                    ,6007
                                                    ,5938)
        GROUP BY 1,2) B
        ON CAST(A.SKU_NBR AS INT)=CAST(B.SKU_NBR AS INT)
        AND CAST(A.LOC_NBR AS INT)= CAST(TRIM(B.LOC_NBR) AS INT)
    FULL OUTER JOIN 
        (SELECT 
            ITEM_ID AS SKU_NBR,
            DC_LOC_ID (int) AS LOC_NBR, 
            SUM(ITEM_QTY) AS CZT_ORD  
        FROM PR_GSC_DCM_VIEWS.INB_DC_SOQ_NK
        WHERE DC_LOC_ID IN (6707
                                                          ,6705
                                                        ,6777
                                                        ,6760
                                                        ,6006
                                                         ,6007
                                                        ,5938)
        GROUP BY 1,2 ) C
        ON CAST(A.SKU_NBR AS INT)=CAST(C.SKU_NBR AS INT)
        AND CAST(A.LOC_NBR AS INT)= CAST(C.LOC_NBR AS INT)
    FULL OUTER JOIN  
        (SELECT  
            COALESCE(DEST_SKU_NBR,SKU_NBR) AS SKU_NBR, 
            RECV_LOC_NBR (int) AS LOC_NBR , 
            SUM(INTRANSIT_QTY+OO_NOTSHIPPED_QTY) AS OO
        FROM  PR_DCREPL_CSTM_VIEWS.DFC_PRODUCTONORDER_HIST
        WHERE RECV_LOC_NBR IN (6707
                                                                ,6705
                                                                ,6777
                                                                ,6760
                                                                ,6006
                                                                ,6007
                                                                ,5938)
            AND CAL_DT = DATE
        GROUP BY 1,2) D
        ON CAST(A.SKU_NBR AS INT)=CAST(D.SKU_NBR AS INT)
        AND CAST(A.LOC_NBR AS INT)= CAST(TRIM(D.LOC_NBR )AS INT) 
    FULL OUTER JOIN 
        (
        SELECT 
            LOC_NBR
            , THD_SO_SKU_NBR AS SKU_NBR
            ,SUM(EXPCTD_ALLOC_QTY) AS VIR_ALLOC_OO
        FROM PR_US_INV_VIEWS.LOC_ITEM_VALLOC A
        LEFT OUTER JOIN (SELECT * FROM PR_US_MASTER_VIEWS.LOCATION WHERE DATE BETWEEN EFF_BGN_DT AND EFF_END_DT) AS D
            ON A.LOC_ID = D.LOC_ID
        LEFT OUTER JOIN (SELECT * FROM PR_US_MASTER_VIEWS.ITEM WHERE DATE BETWEEN EFF_BGN_DT AND EFF_END_DT) AS E
            ON A.ITEM_ID = E.ITEM_ID
        INNER JOIN (SELECT DISTINCT THD_SO_SKU_NBR,THD_SKU_NBR FROM  PR_DCREPL_CSTM_STG.DFC_SHRD_SKU) F
            ON E.ITEM_SKU_NBR=F.THD_SKU_NBR
        WHERE LOAD_DT = (SELECT MAX(LOAD_DT) FROM PR_US_INV_VIEWS.LOC_ITEM_VALLOC)
            AND LOC_NBR IN (6707,6760,6777,6705,6007,6006,5938)
            group by 1,2) E
        ON CAST(A.SKU_NBR AS INT)=CAST(E.SKU_NBR AS INT)
        AND CAST(A.LOC_NBR AS INT)= CAST(TRIM(E.LOC_NBR )AS INT)         
    
    
    
    
    GROUP BY 1,2) A
	
'
,@dest_tbl = 'DF_STAGE.DBO.NKA_OO_REPORT'
,@crt_dest_tbl = 1;

PRINT 'CAMPUS OH STAGING STARTED'

IF OBJECT_ID('DF_STAGE.DBO.DFC_SKU_INV_SHRD_SKU', 'U') IS NOT NULL DROP TABLE DF_STAGE.DBO.DFC_SKU_INV_SHRD_SKU; 
exec SSISDB.MDDataTech.usp_run_data_transfer
@src_sys = 'NXA1964_DCMprod1', @dest_sys = WAPRCN026B
,@src_qry = ' 
		
	
SELECT* FROM PR_DCREPL_CSTM_STG.DFC_SHRD_SKU 

'
,@dest_tbl = 'DF_STAGE.DBO.DFC_SKU_INV_SHRD_SKU'
,@crt_dest_tbl = 1;

IF OBJECT_ID('DF_STAGE.DBO.DFC_SKU_INV_SDC_DFC_PILOT', 'U') IS NOT NULL DROP TABLE DF_STAGE.DBO.DFC_SKU_INV_SDC_DFC_PILOT; 
exec SSISDB.MDDataTech.usp_run_data_transfer
@src_sys = 'NXA1964_DCMprod1', @dest_sys = WAPRCN026B
,@src_qry = ' 
		
	

SELECT* FROM Pr_dcrepl_cstm_stg.sdc_dfc_pilot

'
,@dest_tbl = 'DF_STAGE.DBO.DFC_SKU_INV_SDC_DFC_PILOT'
,@crt_dest_tbl = 1;



----CAMPUS OH-------
IF OBJECT_ID('DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT', 'U') IS NOT NULL DROP TABLE DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT; 
exec SSISDB.MDDataTech.usp_run_data_transfer
@src_sys = 'NXA1964_DCMprod1', @dest_sys = WAPRCN026B
,@src_qry = ' 
		
	
	SELECT 
		SKU_NBR
		,SUM(CASE WHEN LOC_NBR=6777 THEN OH ELSE 0 END) AS OH6777
		,SUM(CASE WHEN LOC_NBR=6707 THEN OH ELSE 0 END) AS OH6707
		,SUM(CASE WHEN LOC_NBR=6007 THEN OH ELSE 0 END) AS OH6007
		,SUM(CASE WHEN LOC_NBR=6705 THEN OH ELSE 0 END) AS OH6705
		,SUM(CASE WHEN LOC_NBR=6760 THEN OH ELSE 0 END) AS OH6760
		,SUM(CASE WHEN LOC_NBR=6006 THEN OH ELSE 0 END) AS OH6006
		,SUM(CASE WHEN LOC_NBR=5938 THEN OH ELSE 0 END) AS OH5938
		FROM
	(SELECT
		PROD.PRODUCTCODE AS SKU_NBR,
		DFC_LOC_NBR AS LOC_NBR,
		SUM(QTY) AS OH
    FROM PR_DCREPL_CSTM.DFC_INV_DLY_HIST POH
	
    JOIN PR_DCMREPL_views.PRODUCT PROD
         ON PROD.PRODUCTCODE = CAST(POH.SKU_NBR AS VARCHAR(20))
   WHERE (INV_TYP = ''ALLOCATABLE''
          OR (INV_TYP = ''UNALLOCATABLE'' AND LOCK_CD IN
                                          (SELECT SSYS_PARM_CHAR_VAL FROM PR_DCREPL_CSTM.SSYS_PARM
                                           WHERE SUB_SYS_CD   = ''DF''
                                             AND SSYS_PARM_NM = ''DFC_OH_LOCK_CD'')
                    )
                    )
          AND POH.CAL_DT = (SELECT MAX(CAL_DT) FROM PR_DCREPL_CSTM.DFC_INV_DLY_HIST)
		  GROUP BY 1,2) A
		  GROUP BY 1
'
,@dest_tbl = 'DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT'
,@crt_dest_tbl = 1;

PRINT 'CAMPUS OH FINAL STARTED'

IF OBJECT_ID('DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT_FINAL', 'U') IS NOT NULL DROP TABLE DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT_FINAL; 

SELECT * INTO DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT_FINAL
FROM(
	SELECT 
		SKU_NBR, 
		6007 AS LOC_NBR,
		OH6007+OH6006 AS CAMPUS_OH
	FROM DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT

	UNION ALL 

	SELECT 
		SKU_NBR, 
		6006 AS LOC_NBR,
		OH6007+OH6006 AS CAMPUS_OH
	FROM DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT

	UNION ALL

	SELECT 
		SKU_NBR, 
		6777 AS LOC_NBR,
		OH6777+OH6705 AS CAMPUS_OH
	FROM DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT

	UNION ALL 

	SELECT 
		SKU_NBR, 
		6705 AS LOC_NBR,
		OH6777+OH6705 AS CAMPUS_OH
	FROM DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT

	UNION ALL

	SELECT 
		SKU_NBR, 
		5938 AS LOC_NBR,
		OH5938 AS CAMPUS_OH
	FROM DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT

	UNION ALL

	SELECT 
		SKU_NBR, 
		6760 AS LOC_NBR,
		OH6760 AS CAMPUS_OH
	FROM DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT

	UNION ALL

	SELECT 
		SKU_NBR, 
		6707 AS LOC_NBR,
		OH6707 AS CAMPUS_OH
	FROM DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT
) A ;


PRINT 'OH_OO_FINAL STARTED' 

IF OBJECT_ID('DF_STAGE.DBO.NKA_OH_OO_FINAL', 'U') IS NOT NULL DROP TABLE DF_STAGE.DBO.NKA_OH_OO_FINAL; 

SELECT * INTO DF_STAGE.DBO.NKA_OH_OO_FINAL
FROM(
SELECT 
	COALESCE(CAST(A.SKU_NBR AS INT), CAST(B.SKU_NBR AS INT)) AS SKU_NBR,
	COALESCE(CAST(A.LOC_NBR AS INT),CAST(B.LOC_NBR AS INT)) AS LOC_NBR,
	CAMPUS_OH,
	DCM_OO,
	TOTAL_OO
 FROM DF_STAGE.DBO.NKA_CAMPUS_OH_REPORT_FINAL A
FULL OUTER JOIN DF_STAGE.DBO.NKA_OO_REPORT B
ON CAST(A.SKU_NBR AS INT)= CAST(B.SKU_NBR AS INT)
AND CAST(A.LOC_NBR AS INT)=CAST(B.LOC_NBR AS INT))A;

PRINT 'OH_OO_FINAL_COMPLETE'


---------------------

SELECT [CAL_DT]
      ,[UPDATED_TS]
      ,[Sub Dept]
      ,[Class]
      ,[Class Desc]
      ,[Subclass]
      ,[Subclass Desc]
      ,[OMSID]
      ,[SKU Number]
      ,[SKU Desc]
      ,[MKT 401 SKU Status]
      ,[DC Number]
      ,[DC Name]
      ,[DC SKU Status]
      ,[CURR_DSVC_TYP_CD]
      ,[MVNDR Number]
        ,[MVNDR Name]
              ,[ALT MVNDR Number]
          , C.MVNDR_NM AS "ALT MVNDR Name"
      ,[Import Domestic Ind]
      ,[MFG Part #]
      ,[Buypack_Qty]
      ,[DSVC Type Code]
      ,[Current Cost Amount]
      ,[Current Retail Amount]
      ,[OH Qty]
      ,[Avail OH Qty]
      ,[OO Qty]
      ,[Confirmed Allocation Qty]
      ,[Expected Allocation Qty]

, CASE WHEN CONCAT (
CASE WHEN LEFT(REVIEWDAYS, 1) >0 THEN 'M' ELSE NULL END
,CASE WHEN RIGHT(LEFT(REVIEWDAYS, 2),1) >0 THEN 'Tue' ELSE NULL END
,CASE WHEN RIGHT(LEFT(REVIEWDAYS, 3),1) >0 THEN ', W' ELSE NULL END
,CASE WHEN RIGHT(LEFT(REVIEWDAYS, 4),1) >0 THEN ', Thr' ELSE NULL END
,CASE WHEN RIGHT(LEFT(REVIEWDAYS, 5),1) >0 THEN ', F' ELSE NULL END
,CASE WHEN RIGHT(LEFT(REVIEWDAYS, 6),1) >0 THEN ', Sat' ELSE NULL END
,CASE WHEN RIGHT(LEFT(REVIEWDAYS, 7),1) >0 THEN ', Sun' ELSE NULL END) = '' THEN '-'ELSE 
CONCAT (
CASE WHEN LEFT(REVIEWDAYS, 1) >0 THEN 'M' ELSE NULL END
,CASE WHEN RIGHT(LEFT(REVIEWDAYS, 2),1) >0 THEN 'Tue' ELSE NULL END
,CASE WHEN RIGHT(LEFT(REVIEWDAYS, 3),1) >0 THEN ', W' ELSE NULL END
,CASE WHEN RIGHT(LEFT(REVIEWDAYS, 4),1) >0 THEN ', Thr' ELSE NULL END
,CASE WHEN RIGHT(LEFT(REVIEWDAYS, 5),1) >0 THEN ', F' ELSE NULL END
,CASE WHEN RIGHT(LEFT(REVIEWDAYS, 6),1) >0 THEN ', Sat' ELSE NULL END
,CASE WHEN RIGHT(LEFT(REVIEWDAYS, 7),1) >0 THEN ', Sun' ELSE NULL END)END
AS "Review Days"
,OUTL_QTY AS TOPUP
--,OUTL_RET AS TOPUP_RETAIL
,ROUNDED_SOQ_QTY AS SOQ
--,ROUNDED_SOQ_RET AS SOQ_RETAIL

INTO #FINAL
--INTO #TEMP
FROM DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_BYP_SKU_INV A
LEFT JOIN (SELECT LOCATIONID, VENDORNUMBER, MAX(REVIEWDAYS) AS REVIEWDAYS FROM DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV_REVIEWDAYS
GROUP BY LOCATIONID, VENDORNUMBER) B
ON "DC NUMBER" = RIGHT("LOCATIONID", 4) 
AND "MVNDR NUMBER" = RIGHT(VENDORNUMBER,5)
LEFT JOIN DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV_MVNDR_ORG C
ON A."ALT MVNDR NUMBER" = C.MVNDR_NBR 
LEFT JOIN #T2 D
ON A.[SKU Number] = D.SKU 
AND A.[DC Number] = D.DC 


--update DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV
--set "OO Qty" = case when b."qty ordered" is null then 0 else b."qty ordered"  end
--from 
--DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV a
--left join (
--select sku, dc, sum("QTY ORDERED") as "QTY ORDERED" from  DF_STAGE.dbo.DF_ANALYTICS_DB_open_PO_DETAIL WHERE "LINE STATUS" = 'Open' group by "sku", dc
--) b
--on a.[DC Number] = b.DC
--and a.[SKU Number] = b.SKU 

PRINT 'FINAL DFC SKU INV STARTED'



IF OBJECT_ID('DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV_STG', 'U') IS NOT NULL
DROP TABLE  DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV_STG

SELECT [CAL_DT]
      ,[UPDATED_TS]
      ,[Sub Dept]
      ,[Class]
      ,[Class Desc]
      ,[Subclass]
      ,[Subclass Desc]
      ,[OMSID]
      ,[SKU Number]
      ,[SKU Desc]
      ,[MKT 401 SKU Status]
      ,[DC Number]
	  ,CAMPUS_OH AS "Campus OH Qty"
      ,[DC Name]
      ,[DC SKU Status]
      ,[CURR_DSVC_TYP_CD]
      ,[MVNDR Number]
      ,[MVNDR Name]
      ,[ALT MVNDR Number]
      ,[ALT MVNDR Name]
      ,[Import Domestic Ind]
      ,[MFG Part #]
      ,[Buypack_Qty]
      ,COALESCE (K.DSVC_TYPE_CD, I.DSVC_TYP_DESC, 'DFC') AS [DSVC Type Code]
      ,[Current Cost Amount]
      ,[Current Retail Amount]
      ,[OH Qty]
      ,[Avail OH Qty]
	  ,DCM_OO AS "DCM OO Qty"
      ,TOTAL_OO AS [OO Qty]--total oo
      ,[Confirmed Allocation Qty]
      ,[Expected Allocation Qty]
      ,[Review Days]
      ,[TOPUP]
      ,[SOQ]

	  INTO 	  [DF_STAGE].[dbo].[DF_ANALYTICS_DB_DFC_SKU_INV_STG]

 FROM #FINAL A
  LEFT JOIN  DF_STAGE.DBO.NKA_OH_OO_FINAL B
  ON A."SKU NUMBER" = B.SKU_NBR
  AND A."DC NUMBER" = B.LOC_NBR 


LEFT JOIN (
SELECT DF_MVNDR_NBR, RIGHT(DFC_LOC_NBR,4) AS DFC_LOC_NUMBER, THD_SO_SKU_NBR
, CASE WHEN DSVC_TYP_CD IN (2,3) THEN 'RDC' WHEN DSVC_TYP_CD IN (4,6) THEN 'SDC' ELSE 'DFC' END AS DSVC_TYP_DESC 
FROM  DF_STAGE.DBO.DFC_SKU_INV_SHRD_SKU
) I	
ON A."SKU NUMBER" = I.THD_SO_SKU_NBR
AND A.[MVNDR Number] = I.DF_MVNDR_NBR 
AND A."DC NUMBER" = right(i.DFC_LOC_NUMBER,4)

LEFT JOIN (SELeCT K.*, 'SDC' AS DSVC_TYPE_CD 
FROM DF_STAGE.DBO.DFC_SKU_INV_SDC_DFC_PILOT K
) K
ON a.[MVNDR Number] = K.MVNDR_NBR
AND a."SKU NUMBER" = K.THD_SO_SKU_NBR
AND a."DC NUMBER" = right(k.LOC_NBR,4)

  
    UPDATE SKU_INV_STG
    
SET
    [Import Domestic Ind] = CASE WHEN D_TYP_CD = 'DF - Imp' THEN 'I' ELSE 'D' END
FROM
    DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV_STG SKU_INV_STG
INNER JOIN
    DF_STAGE.dbo.DF_ANALYTICS_DB_OMSID_IMP_IND IMP_IND
ON 
    SKU_INV_STG.[MVNDR Number] = IMP_IND.MVNDR_NBR;



  IF OBJECT_ID('DF_ANALYTICS_DB_DFC_SKU_INV_PREV_DT', 'U') IS NOT NULL
  drop table DF_ANALYTICS_DB_DFC_SKU_INV_PREV_DT
  exec sp_rename 'DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV', 'DF_ANALYTICS_DB_DFC_SKU_INV_PREV_DT'

  exec sp_rename 'DF_STAGE.DBO.DF_ANALYTICS_DB_DFC_SKU_INV_STG', 'DF_ANALYTICS_DB_DFC_SKU_INV'

    PRINT 'DFC SKU INV - FINAL - COMPLETE!!!!'

	
	


IF OBJECT_ID('DF_STAGE.DBO.DF_ANALYTICS_DB_SDC_SOURCED_SKUS', 'U') IS NOT NULL DROP TABLE DF_STAGE.DBO.DF_ANALYTICS_DB_SDC_SOURCED_SKUS; 
exec SSISDB.MDDataTech.usp_run_data_transfer
@src_sys = 'NXA1964_DCMprod1', @dest_sys = WAPRCN026B
,@src_qry = ' 
		
	
SELECT MVNDR_NBR AS "MVNDR Number", THD_SO_SKU_NBR "OMS THD SKU", THD_SKU_NBR "CORE SKU", YOW_SKU_NBR AS "YOW SKU", LOC_NBR AS "DC Number", CRT_TS "Create Timestamp"
FROM pr_dcrepl_cstm_stg.sdc_dfc_pilot


'
,@dest_tbl = 'DF_STAGE.DBO.DF_ANALYTICS_DB_SDC_SOURCED_SKUS'
,@crt_dest_tbl = 1;


PRINT 'AD HOC DCM PROD FOR NAND KISHORE COMPLETE'



----REMOVE FROM HERE ONCE KARTHIK V CREATES BQ CONNECTION AND ADD BACK TO DF_INSTOCK_DLY STORED PROC
DROP TABLE [DF_STAGE].[dbo].[DF_TABLEAU_DFC_IS_YOY_EMAIL_OOS]
exec SSISDB.MDDataTech.usp_run_data_transfer
@src_sys = 'PXP2731_EDWPROD1', @dest_sys = WAPRCN026B,@src_qry = 
'

SEL oos_cal_dt,item_grp_cd, item_grp_desc--, item_class_cd, item_class_desc, 
,AVG(sku_maintenance_pct) AS SKU_Maint,AVG(fill_rate_pct) AS fill_rate,AVG(late_po_pct) AS late_po,AVG(low_order_pct) AS low_order,AVG(excess_sales_pct) AS sales_greater_fcst,AVG(oh_change_pct) AS inv_adj,AVG(unknown_pct) AS other
FROM  lab_ipr.dfc_oos_data_quality_report
WHERE oos_cal_dt = CURRENT_DATE -2
GROUP BY 1,2,3;--,4,5;


'
,@dest_tbl = '[DF_STAGE].[dbo].[DF_TABLEAU_DFC_IS_YOY_EMAIL_OOS]'
,@crt_dest_tbl = 1




DROP TABLE [DF_STAGE].[dbo].[DF_TABLEAU_DFC_IS_YOY_EMAIL_OOS_tot]
exec SSISDB.MDDataTech.usp_run_data_transfer
@src_sys = 'PXP2731_EDWPROD1', @dest_sys = WAPRCN026B,@src_qry = 
'

SEL oos_cal_dt
,AVG(sku_maintenance_pct) AS SKU_Maint,AVG(fill_rate_pct) AS fill_rate,AVG(late_po_pct) AS late_po,AVG(low_order_pct) AS low_order,AVG(excess_sales_pct) AS sales_greater_fcst,AVG(oh_change_pct) AS inv_adj,AVG(unknown_pct) AS other
FROM  lab_ipr.dfc_oos_data_quality_report
WHERE oos_cal_dt = CURRENT_DATE -2
GROUP BY 1;--,4,5;


'
,@dest_tbl = '[DF_STAGE].[dbo].[DF_TABLEAU_DFC_IS_YOY_EMAIL_OOS_tot]'
,@crt_dest_tbl = 1

PRINT 'DF INSTOCK ROOT CAUSE STAGING COMPLETE'

end

ELSE 
PRINT 'Not Processing DFC SKU INV - DATA UNAVAIALBLE IN BQ'


END
